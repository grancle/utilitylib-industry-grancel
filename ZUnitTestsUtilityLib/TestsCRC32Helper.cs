﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UtilityLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsUtilityLib
{
    [TestClass()]
    public class TestsCRC32Helper
    {
        [TestMethod()]
        public void Testcrc32()
        {
            var obj = new CRC32Helper(new byte[] { 0x31, 0x32, 0x33, 0x34 });
            byte[] val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0x9B, val[0]);
            Assert.AreEqual(0xE3, val[1]);
            Assert.AreEqual(0xE0, val[2]);
            Assert.AreEqual(0xA3, val[3]);

            obj = new CRC32Helper(new byte[] { 0x01, 0x02, 0x03, 0x04 });
            val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0xB6, val[0]);
            Assert.AreEqual(0x3C, val[1]);
            Assert.AreEqual(0xFB, val[2]);
            Assert.AreEqual(0xCD, val[3]);

            obj = new CRC32Helper(new byte[] { 0x99, 0x68, 0x1A, 0x64, 0x21, 0x03, 0x86, 0xF5 });
            val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0x99, val[0]);
            Assert.AreEqual(0x8D, val[1]);
            Assert.AreEqual(0xCD, val[2]);
            Assert.AreEqual(0x1D, val[3]);

            obj = new CRC32Helper(new byte[] { 0x21, 0x03, 0x86, 0xF5, 0x06, 0xE2, 0xB9, 0xA3, 0x1A, 0x64, 0x21, 0x03});
            val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0x6B, val[0]);
            Assert.AreEqual(0x27, val[1]);
            Assert.AreEqual(0x41, val[2]);
            Assert.AreEqual(0x76, val[3]);

            obj = new CRC32Helper(new byte[] { 0x06, 0xE2, 0xB9, 0xA3, 0xA3 });
            val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0x35, val[0]);
            Assert.AreEqual(0x6E, val[1]);
            Assert.AreEqual(0x53, val[2]);
            Assert.AreEqual(0xE9, val[3]);

            obj = new CRC32Helper(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF });
            val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0xFF, val[0]);
            Assert.AreEqual(0xFF, val[1]);
            Assert.AreEqual(0xFF, val[2]);
            Assert.AreEqual(0xFF, val[3]);

            obj = new CRC32Helper(new byte[] { 0x00, 0x00, 0x00, 0x00 });
            val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0x21, val[0]);
            Assert.AreEqual(0x44, val[1]);
            Assert.AreEqual(0xDF, val[2]);
            Assert.AreEqual(0x1C, val[3]);

            obj = new CRC32Helper(new byte[] { 0x08, 0x32, 0xFF, 0x00 });
            val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0x50, val[0]);
            Assert.AreEqual(0x3B, val[1]);
            Assert.AreEqual(0x3B, val[2]);
            Assert.AreEqual(0x7F, val[3]);

            obj = new CRC32Helper(new byte[] { 0xFF, 0x00, 0x00, 0xFF });
            val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0xD2, val[0]);
            Assert.AreEqual(0x43, val[1]);
            Assert.AreEqual(0x36, val[2]);
            Assert.AreEqual(0x60, val[3]);

            obj = new CRC32Helper(new byte[] { 0x00, 0xFF, 0xFF, 0x00 });
             val = obj.crc32();
            Assert.IsNotNull(val);
            Assert.AreEqual(4, val.Length);
            Assert.AreEqual(0x0C, val[0]);
            Assert.AreEqual(0xF8, val[1]);
            Assert.AreEqual(0x16, val[2]);
            Assert.AreEqual(0x83, val[3]);
        }

        [TestMethod()]
        public void Testcrc16_modbus()
        {
            var obj = new CRC32Helper(new byte[] { 0x0A, 0x01 });
            byte[] val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0xD0, val[0]);
            Assert.AreEqual(0xC6, val[1]);

            obj = new CRC32Helper(new byte[] { 0x31, 0x32, 0x33, 0x34 });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0x30, val[0]);
            Assert.AreEqual(0xBA, val[1]);

            obj = new CRC32Helper(new byte[] { 0x01, 0x02, 0x03, 0x04 });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0x2B, val[0]);
            Assert.AreEqual(0xA1, val[1]);

            obj = new CRC32Helper(new byte[] { 0x99, 0x68, 0x1A, 0x64, 0x21, 0x03, 0x86, 0xF5 });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0x22, val[0]);
            Assert.AreEqual(0x4B, val[1]);

            obj = new CRC32Helper(new byte[] { 0x21, 0x03, 0x86, 0xF5, 0x06, 0xE2, 0xB9, 0xA3, 0x1A, 0x64, 0x21, 0x03 });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0x9B, val[0]);
            Assert.AreEqual(0x49, val[1]);

            obj = new CRC32Helper(new byte[] { 0x06, 0xE2, 0xB9, 0xA3, 0xA3 });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0x14, val[0]);
            Assert.AreEqual(0x73, val[1]);

            obj = new CRC32Helper(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0xB0, val[0]);
            Assert.AreEqual(0x01, val[1]);

            obj = new CRC32Helper(new byte[] { 0x00, 0x00, 0x00, 0x00 });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0x24, val[0]);
            Assert.AreEqual(0x00, val[1]);

            obj = new CRC32Helper(new byte[] { 0x08, 0x32, 0xFF, 0x00 });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0x7B, val[0]);
            Assert.AreEqual(0xE2, val[1]);

            obj = new CRC32Helper(new byte[] { 0xFF, 0x00, 0x00, 0xFF });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0x70, val[0]);
            Assert.AreEqual(0x70, val[1]);

            obj = new CRC32Helper(new byte[] { 0x00, 0xFF, 0xFF, 0x00 });
            val = obj.crc16_modbus();
            Assert.IsNotNull(val);
            Assert.AreEqual(2, val.Length);
            Assert.AreEqual(0xE4, val[0]);
            Assert.AreEqual(0x71, val[1]);
        }
    }
}