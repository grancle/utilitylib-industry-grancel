﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class ListHelper
    {

        public static void Update<T>(this List<T> list, Action<T> actionUpdate, Func<T, bool> conditionUpdate, T newValue)
        {
            bool update = list.DoSomethingForEachBreak(actionUpdate, conditionUpdate);
            if (update)
            {
                return;
            }
            list.Add(newValue);
        }

        public static ObservableCollection<T> ToCollection<T>(this List<T> list)
        {
            if(list == null)
            {
                return null;
            }
            ObservableCollection<T> result = new ObservableCollection<T>();
            if(list.Count == 0)
            {
                return result;
            }
            list.ForEach((item) =>
            {
                result.Add(item);
            });
            return result;
        }

        public static void AddRangeWithCapacity<T>(this List<T> list, IEnumerable<T> collection, int capacity = 1024)
        {
            if (list == null || collection == null)
            {
                return;
            }
            if (capacity <= 0)
            {
                capacity = 1024;
            }
            try
            {
                list.AddRange(collection);
            }
            catch { }
            if (list.Count > capacity)
            {
                int count = collection.Count();
                while(count>0 && list.Count> count)
                {
                    list.RemoveAt(0);
                }
            }
        }

        public static void AddWithCapacity<T>(this List<T> list, T item, int capacity = 1024)
        {
            if (list == null)
            {
                return;
            }
            if (capacity <= 0)
            {
                capacity = 1024;
            }
            try
            {
                list.Add(item);
            }
            catch { }
            if(list.Count > capacity)
            {
                list.RemoveAt(0);
            }
        }

        public static byte[] ConvertToByteArray(this List<byte[]> list)
        {
            if(list == null)
            {
                return null;
            }
            List<byte> result = new List<byte>();
            list.DoSomethingForEach((item) =>
            {
                try
                {
                    result.AddRange(item);
                }
                catch { }
            });

            return result.ToArray();
        }
    }
}
