﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class LongNumberHelper
    {
        public LongNumberHelper(string MyInfo, string MySuffix = null)
        {
            this.MyInfo = MyInfo;
            if(MySuffix != null)
            {
                this.MySuffix = MySuffix;
            }
            ConvertBackToNumber();
        }

        public LongNumberHelper(decimal MyNumber)
        {
            this.MyNumber = MyNumber;
            ConvertToAddSymbol(this.MyNumber);
            //CalDigit(this.MyNumber);
        }


        //public LongNumberHelper(object MyNumberObj)
        //{
        //    ConvertToAddSymbol(MyNumberObj);
        //}

        public string MyInfo { get; private set; } = string.Empty;

        public decimal MyNumber { get; private set; } = 0;

        public string MyNumberStr { get; private set; } = string.Empty;

        public string MySuffix { get; private set; } = string.Empty;

        public string MyUnit { get; private set; } = string.Empty;

        public int MyUnitDigit { get; private set; } = 0;

        public decimal MyNumberWithUnit { get; private set; } = 0;

        //public string MyInfoWithUnit
        //{
        //    get
        //    {
        //        return $"{MyNumberWithUnit} {MyUnit}{MySuffix}";
        //    }
        //}

        public int DIGIT_SAVE_AT_POINT_RIGHT { get; set; } = 5;

        public int MyDecimalDigit { get; private set; } = 0;

        private void ConvertBackToNumber()
        {
            try
            {
                ConvertBackToNumberImpl();
            }
            catch { }
        }

        private void ConvertBackToNumberImpl()
        {
            if (string.IsNullOrEmpty(MyInfo))
            {
                MyNumber = 0;
                return;
            }
            string num_str = this.MyInfo;
            if (num_str.EndsWith("\r\n"))
            {
                num_str = num_str.Replace("\r\n", "");
            }
            else if (num_str.EndsWith("\n"))
            {
                num_str = num_str.Replace("\n", "");
            }
            else if (num_str.EndsWith("\r"))
            {
                num_str = num_str.Replace("\r", "");
            }

            num_str = num_str.Replace(",", "");
            if (!string.IsNullOrEmpty(MySuffix) && num_str.EndsWith(MySuffix, StringComparison.OrdinalIgnoreCase))
            {
                num_str = num_str.Substring(0, num_str.Length - 2);
            }
            int power = 0;
            if (num_str.EndsWith("m", StringComparison.OrdinalIgnoreCase))
            {
                power = 6;
                MyUnit = "M";
                num_str = num_str.Substring(0, num_str.Length - 1);
            }
            else if (num_str.EndsWith("k", StringComparison.OrdinalIgnoreCase))
            {
                power = 3;
                MyUnit = "K";
                num_str = num_str.Substring(0, num_str.Length - 1);
            }
            else if (num_str.EndsWith("g", StringComparison.OrdinalIgnoreCase))
            {
                power = 9;
                MyUnit = "G";
                num_str = num_str.Substring(0, num_str.Length - 1);
            }
            MyUnitDigit = power;
            num_str = num_str.Trim();
            decimal result = Formulars.Instance.ConvertStrToDecimal(num_str);
            MyNumberWithUnit = result;
            if (power > 0)
            {
                result = result * (decimal)Math.Pow(10, power);
            }
            MyNumber = result;

            ConvertToAddSymbol(result);
        }

        private void ConvertToAddSymbol(object num)
        {
            string num_str = string.Empty;
            try
            {
                num_str = num.ToString();
            }
            catch
            {
                this.MyNumberStr = num_str;
                return;
            }

            if (!num_str.Contains("."))
            {
                this.MyNumberStr = AddThousandthSymbol(num_str);
                return;
            }

            string[] num_array = num_str.Split('.');
            if (num_array == null || num_array.Length != 2)
            {
                this.MyNumberStr = string.Empty;
                return;
            }
            string num_left = num_array[0];
            string num_right = num_array[1];
            num_left = AddThousandthSymbol(num_left);
            num_right = AddThousandthSymbol(num_right, false);
            this.MyNumberStr = $"{num_left}.{num_right}";

            _num_left = num_left;
            _num_right = num_right;
            //if(string.IsNullOrEmpty(this.MyInfo))
            //{
            //    this.MyInfo = this.MyNumberStr;
            //}
        }


        private string _num_left = string.Empty;
        private string _num_right = string.Empty;

        private string AddThousandthSymbol(string src, bool isAddInt = true)
        {
            if (string.IsNullOrEmpty(src))
            {
                return string.Empty;
            }

            if(src.Contains('.'))
            {
                while (src.EndsWith("0"))
                {
                    src = src.Substring(0, src.Length - 1);
                    if (src.Length <= DIGIT_SAVE_AT_POINT_RIGHT)
                    {
                        break;
                    }
                }
            }
 

            int index = 0;
            char[] char_values = src.ToCharArray();
            StringBuilder sb = new StringBuilder();
            if (isAddInt)
            {
                Stack<char> stack = new Stack<char>();
                for (int i = char_values.Length - 1; i >= 0; i--)
                {
                    char item = char_values[i];
                    AddThousandthSymbolSub(ref stack, ref index, item);
                }
                sb = ConvertStackCharToStrBuilder(stack);
            }
            else
            {
                for (int i = 0; i < char_values.Length; i++)
                {
                    char item = char_values[i];
                    AddThousandthSymbolSub(ref sb, ref index, item);
                }
            }

            string result = sb.ToString();
            if (result.StartsWith(","))
            {
                result = result.Substring(1, result.Length - 1);
            }
            if (result.EndsWith(","))
            {
                result = result.Substring(0, result.Length - 1);
            }
            return result;
        }

        private void AddThousandthSymbolSub(ref StringBuilder sb, ref int index, char item)
        {
            if (sb == null)
            {
                sb = new StringBuilder();
            }
            sb.Append(item);
            index++;
            if (index < 3)
            {
                return;
            }
            sb.Append(',');
            index = 0;
        }

        private void AddThousandthSymbolSub(ref Stack<char> sb, ref int index, char item)
        {
            if (sb == null)
            {
                sb = new Stack<char>();
            }
            sb.Push(item);
            index++;
            if (index < 3)
            {
                return;
            }
            sb.Push(',');
            index = 0;
        }

        private StringBuilder ConvertStackCharToStrBuilder(Stack<char> stack)
        {
            if (stack == null || stack.Count == 0)
            {
                return new StringBuilder();
            }

            StringBuilder result = new StringBuilder();
            while (stack.Count > 0)
            {
                char item = stack.Pop();
                result.Append(item);
            }
            return result;
        }

        private void CalDigit(decimal myNumber)
        {
            try
            {
                CalDigitImpl(myNumber);
            }
            catch { }
        }

        private void CalDigitImpl(decimal myNumber)
        {
            string num_str = myNumber.ToString();
            if(!num_str.Contains('.'))
            {
                MyDecimalDigit = 0;
                return;
            }
            num_str = num_str.Trim('0');
            int point = num_str.IndexOf('.');
            point = num_str.Length - point;
            MyDecimalDigit = num_str.Length - point -1;
        }

        public decimal CalRandomNumber(ref Random random)
        {
            if (random == null)
            {
                return 0;
            }
            try
            {
                return CalRandomNumberImpl(ref random);
            }
            catch { }

            return 0;
        }
        private decimal CalRandomNumberImpl(ref Random random)
        {
            CalDigit(this.MyNumber);
            int max = (int)(this.MyNumber * (int)Math.Pow(10, this.MyDecimalDigit));
            int min = max * -1;
            int result_int = random.Next(min, max);
            decimal result = (decimal)result_int * (decimal)Math.Pow(10, this.MyDecimalDigit * -1);
            return result;
        }

        public decimal CalPPM(decimal ppm)
        {
            try
            {
                ppm = ppm * (decimal)Math.Pow(10, -6);
                return this.MyNumber * ppm;
            }
            catch
            {
                return 0;
            }
        }

        public decimal CalPPB(decimal ppb)
        {
            try
            {
                ppb = ppb * (decimal)Math.Pow(10, -9);
                return this.MyNumber * ppb;
            }
            catch
            {
                return 0;
            }
        }

        public override string ToString()
        {
            return this.MyNumberStr;
        }

        public static LongNumberHelper operator +(LongNumberHelper x, LongNumberHelper y)
        {
            if (ReferenceEquals(x, null) && ReferenceEquals(y, null)) return null;
            if (ReferenceEquals(x, null)) return y;
            if (ReferenceEquals(y, null)) return x;

            decimal sum = x.MyNumber + y.MyNumber;
            LongNumberHelper result = new LongNumberHelper(sum);
            return result;
        }

        public static LongNumberHelper operator -(LongNumberHelper x, LongNumberHelper y)
        {
            if (ReferenceEquals(x, null) && ReferenceEquals(y, null)) return null;
            if (ReferenceEquals(x, null)) return y;
            if (ReferenceEquals(y, null)) return x;

            decimal sum = x.MyNumber - y.MyNumber;
            LongNumberHelper result = new LongNumberHelper(sum);
            return result;
        }

        public static LongNumberHelper operator *(LongNumberHelper x, LongNumberHelper y)
        {
            if (ReferenceEquals(x, null) && ReferenceEquals(y, null)) return null;
            if (ReferenceEquals(x, null)) return y;
            if (ReferenceEquals(y, null)) return x;

            decimal sum = x.MyNumber * y.MyNumber;
            LongNumberHelper result = new LongNumberHelper(sum);
            return result;
        }

        public static LongNumberHelper operator /(LongNumberHelper x, LongNumberHelper y)
        {
            if (ReferenceEquals(x, null) && ReferenceEquals(y, null)) return null;
            if (ReferenceEquals(x, null)) return y;
            if (ReferenceEquals(y, null)) return x;
            if (ReferenceEquals(y.MyNumber, 0)) throw new DivideByZeroException();

            decimal sum = x.MyNumber / y.MyNumber;
            LongNumberHelper result = new LongNumberHelper(sum);
            return result;
        }

        public static LongNumberHelper operator *(LongNumberHelper x, int y)
        {
            if (ReferenceEquals(x, null)) return null;

            decimal sum = x.MyNumber * (decimal)y;
            LongNumberHelper result = new LongNumberHelper(sum);
            return result;
        }

        public static LongNumberHelper operator /(LongNumberHelper x, int y)
        {
            if (ReferenceEquals(x, null)) return null;
            if (ReferenceEquals(y, 0)) throw new DivideByZeroException();

            decimal sum = x.MyNumber / (decimal)y;
            LongNumberHelper result = new LongNumberHelper(sum);
            return result;
        }

        public string MyNumberStrWithDigit(int digit)
        {
            if (digit <= 0)
            {
                return _num_left;
            }

            string right = _num_right;
            int len = right.Length;
            int cnt_dot = right.Count((item) =>
            {
                return item == ',';
            });

            if(len < digit + cnt_dot)
            {
                right = SuppleZero(right, digit);
            }
            else
            {
                right = Substring(right, digit);
            }
            

            //if (digit <= 3)
            //{
            //    right = _num_right;
            //    right = SuppleZero(right, digit);
            //    return $"{_num_left}.{right}";
            //}

            //int cnt_dot = _num_right.Count((item) =>
            //{
            //    return item == ',';
            //});


            //digit = digit + cnt_dot;
            //if (_num_right.Length <= digit)
            //{
            //    right = _num_right;
            //    right = SuppleZero(right, digit);
            //}
            //else
            //{
            //    right = _num_right.Substring(0, digit);
            //}

            if (right.EndsWith(","))
            {
                right = right.Substring(0, right.Length - 1);
            }

            return $"{_num_left}.{right}";
        }

        private string SuppleZero(string src, int digit)
        {
            if (string.IsNullOrEmpty(src))
            {
                return string.Empty;
            }
            string result = src;

            int len = result.Length;
            int cnt_digit = 0;
            int cnt_three = 0;
            int index = 0;
            while(cnt_digit < digit)
            {
                cnt_three++;
                if (index < len)
                {
                    if(cnt_three > 3)
                    {
                        cnt_three = 0;
                    }
                    else
                    {
                        cnt_digit++;
                    }

                    index++;
                    continue;
                }
                index++;
                if (cnt_three > 3)
                {
                    result += ",";
                    cnt_three = 0;
                    continue;
                }
                result += "0";
                cnt_digit++;

            }
            //for (int i = 0; i < len; i++)
            //{
            //    if (cnt <= digit)
            //    {
            //        cnt++;
            //        continue;
            //    }
            //    if (i % 3 == 0)
            //    {
            //        result += ",";
            //        continue;
            //    }
            //    result += "0";
            //    cnt++;
            //}

            return result;
        }

        private string Substring(string src, int digit)
        {
            if (string.IsNullOrEmpty(src))
            {
                return string.Empty;
            }

            char[] array = src.ToArray();
            StringBuilder sb = new StringBuilder();

            int cnt = 0;
            for(int i=0;i<array.Length;i++)
            {
                try
                {
                    char item = array[i];
                    sb.Append(item);
                    if (item == ',') continue;
                }
                catch { }
                cnt++;
                if(cnt == digit)
                {
                    break;
                }
            }


            return sb.ToString();
        }
    }
}
