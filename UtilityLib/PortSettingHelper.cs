﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using LoggerLib;

namespace UtilityLib
{
    public class PortSettingHelper
    {
        public static PortSettingHelper Instance { get; } = new PortSettingHelper();
        public Parity ConvertStrToParity(string value)
        {
            if(string.IsNullOrEmpty(value))
            {
                return Parity.None;
            }
            switch (value.ToLower())
            {
                case "None":
                case "none":
                case "无":
                    return Parity.None;
                case "Odd":
                case "odd":
                case "奇":
                    return Parity.Odd;
                case "Even":
                case "even":
                case "偶":
                    return Parity.Even;
                case "Mark":
                case "mark":
                case "标志":
                    return Parity.Mark;
                case "Space":
                case "space":
                case "空格":
                    return Parity.Space;
                default:
                    return Parity.None;
            }
        }

        public StopBits ConvertStrToStopBits(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return StopBits.One;
            }
            switch (value.ToLower())
            {
                case nameof(StopBits.One):
                case "1":
                    return StopBits.One;
                case nameof(StopBits.OnePointFive):
                case "1.5":
                    return StopBits.OnePointFive;
                case nameof(StopBits.Two):
                case "2":
                    return StopBits.Two;
                default:
                    return StopBits.One;
            }
        }

        public Handshake ConvertStrToHandshake(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return Handshake.None;
            }
            switch (value.ToLower())
            {
                case nameof(Handshake.None):
                case "none":
                case "无":
                    return Handshake.None;
                case nameof(Handshake.XOnXOff):
                case "xon / xoff":
                case "xon/xoff":
                case "xonxoff":
                case "软件":
                    return Handshake.XOnXOff;
                case nameof(Handshake.RequestToSend):
                case "rts":
                case "硬件":
                    return Handshake.RequestToSend;
                case nameof(Handshake.RequestToSendXOnXOff):
                case "rts & xonxoff":
                case "rts & xon / xoff":
                case "rtsxonxoff":
                case "硬件和软件":
                case "硬件控制和软件控制":
                case "同时使用硬件控制和软件控制":
                    return Handshake.RequestToSend;
                default:
                    return Handshake.None;
            }


        }

        public List<string> GetPortNames()
        {
            try
            {
                return SerialPort.GetPortNames().ToList();
            }
            catch (Exception ex)
            {
                //Logger.Instance.Log(new Exception(CoreConfigLib.CoreConfigHelper.Instance.MyConfig.ErrMsg.MyCom.get_available_ports_error));
                //Logger.Instance.Log(new Exception("ERROR: Get available serial port names error!"));
                Logger.Instance.Log(ex);
            }

            return null;
        }
    }
}
