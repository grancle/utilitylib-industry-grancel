﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace UtilityLib
{
    public class WaitingHelper
    {
        public void Wait(DateTime date, Func<bool> isTaskCancle, Action<TimeSpan> countDownAction, Action callBack)
        {
            if(DateTime.Now >= date)
            {
                return;
            }

            AutoResetEvent wait = new AutoResetEvent(false);

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 500;
            timer.Elapsed += (sender, e) =>
            {
                if (isTaskCancle() || DateTime.Now >= date)
                {
                    timer.Stop();
                    wait.Set();
                    return;
                }
                TimeSpan countDown = date - DateTime.Now;
                countDownAction?.Invoke(countDown);
            };
            timer.Start();
            wait.WaitOne();
            countDownAction?.Invoke(TimeSpan.Zero);
            callBack?.Invoke();
        }

        public void WaitMinutes(int minute, Func<bool> isTaskCancle, Action<TimeSpan> countDownAction, Action callBack)
        {
            if(minute <=0)
            {
                return;
            }

            AutoResetEvent wait = new AutoResetEvent(false);
            //int count_down_minutes = minute;
            //int count_down_seconds = 60;
            //int count_milliseconds = 0;
            int interval = 500;
            TimeSpan countDown = TimeSpan.FromMinutes(minute);

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = interval;
            timer.Elapsed += (sender, e) =>
            {
                if (isTaskCancle() || countDown == TimeSpan.Zero)
                {
                    timer.Stop();
                    wait.Set();
                    return;
                }

                //TimeSpan countDown = TimeSpan.FromSeconds(count_down_total);
                countDownAction?.Invoke(countDown);
                LoggerLib.Logger.Instance.Debug($"wait TimeSpan={countDown}, total={minute}");
                countDown = countDown.Add(TimeSpan.FromMilliseconds(-500));
            };
            timer.Start();
            wait.WaitOne();
            countDownAction?.Invoke(TimeSpan.Zero);
            callBack?.Invoke();
        }

        public bool Wait(int expire_time, Func<bool> isTaskCancle, Func<bool> action, Action callBack, int maxTryAction = 1)
        {
            if (expire_time <= 0)
            {
                expire_time = 6 * 60;
            }

            AutoResetEvent wait = new AutoResetEvent(false);
            int count_ms = 0;
            int count_second = 0;
            int count_minute = 0;
            int interval = 500;
            bool result = false;
            int count_try_action = 0;
            bool? is_ok = null;

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = interval;
            timer.Elapsed += (sender, e) =>
            {
                timer.Stop();
                count_ms++;
                if (count_ms >= 2)
                {
                    count_second++;
                    count_ms = 0;
                }
                if (count_second >= 60)
                {
                    count_minute++;
                    count_second = 0;
                }
                if (isTaskCancle() || count_minute > expire_time)
                {
                    wait.Set();
                    return;
                }

                if(count_try_action == 0)
                {
                    is_ok = action?.Invoke();
                }

                count_try_action++;
                if(count_try_action >= maxTryAction)
                {
                    count_try_action = 0;
                }

                if(is_ok == true)
                {
                    result = true;
                    wait.Set();
                    return;
                }

                timer.Start();

            };
            timer.Start();
            wait.WaitOne();
            callBack?.Invoke();
            return result;
        }

        public void WaitMinutes(int minute, Func<bool> isTaskCancle, Action<TimeSpan> countDownAction, Func<bool> action, Action callBack, int maxTryAction = 1)
        {
            if (minute <= 0)
            {
                return;
            }

            AutoResetEvent wait = new AutoResetEvent(false);
            //int count_down_minutes = minute;
            //int count_down_seconds = 60;
            //int count_milliseconds = 0;
            int count_try_action = 0;
            int interval = 500;
            bool? is_ok = null;
            TimeSpan countDown = TimeSpan.FromMinutes(minute);

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = interval;
            timer.Elapsed += (sender, e) =>
            {
                timer.Stop();
                if (isTaskCancle() || countDown == TimeSpan.Zero)
                {
                    wait.Set();
                    return;
                }

                //TimeSpan countDown = TimeSpan.FromSeconds(count_down_total);
                countDownAction?.Invoke(countDown);
                LoggerLib.Logger.Instance.Debug($"wait TimeSpan={countDown}, total={minute}");

                if (count_try_action == 0)
                {
                    is_ok = action?.Invoke();
                }

                count_try_action++;

                if (count_try_action >= maxTryAction)
                {
                    count_try_action = 0;
                }

                if (is_ok == true)
                {
                    countDown = countDown.Add(TimeSpan.FromMilliseconds(-500));
                    timer.Start();
                    return;
                }

                timer.Start();
            };
            timer.Start();
            wait.WaitOne();
            countDownAction?.Invoke(TimeSpan.Zero);
            callBack?.Invoke();
        }

        public bool TryAction(Func<bool> chkCondition, int max_try, int sleep_ms)
        {
            int cnt_try = 0;
            while (cnt_try < max_try)
            {
                if (chkCondition())
                {
                    return true;
                }
                Thread.Sleep(sleep_ms);
                cnt_try++;
            }
            return false;
        }

    }
}
