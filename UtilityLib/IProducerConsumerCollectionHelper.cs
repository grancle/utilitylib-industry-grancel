﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class IProducerConsumerCollectionHelper
    {
        public static void DoSomethingForEach<T>(this IProducerConsumerCollection<T> list, Action<T> action)
        {
            if (list == null || list.Count == 0 || action == null)
            {
                return;
            }
            foreach (var item in list)
            {
                action?.Invoke(item);
            }
        }

        public static bool DoSomethingForEachBreak<T>(this IProducerConsumerCollection<T> list, Action<T> action, Func<T, bool> condition)
        {
            if (list == null || action == null || condition == null)
            {
                return false;
            }
            foreach (var item in list)
            {
                bool chk = condition.Invoke(item);
                if (chk)
                {
                    action.Invoke(item);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// for async while list was modified
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="action"></param>
        public static void DoSomethingForEachInCopies<T>(this IProducerConsumerCollection<T> list, Action<T> action)
        {
            if (list == null || list.Count == 0 || action == null)
            {
                return;
            }
            var array = list.ToArray();
            foreach (var item in array)
            {
                action?.Invoke(item);
            }
            array = null;
            GC.Collect();
        }

        /// <summary>
        /// for async while list was modified
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="action"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static bool DoSomethingForEachBreakInCopies<T>(this IProducerConsumerCollection<T> list, Action<T> action, Func<T, bool> condition)
        {
            if (list == null || action == null || condition == null)
            {
                return false;
            }
            var array = list.ToArray();
            foreach (var item in array)
            {
                bool chk = condition.Invoke(item);
                if (chk)
                {
                    action.Invoke(item);
                    return true;
                }
            }
            return false;
        }
    }
}
