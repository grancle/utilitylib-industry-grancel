﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class ICollectionHelper
    {
        public static void DoSomethingForEach<T>(this ICollection<T> list, Action<T> action)
        {
            if (list == null || list.Count == 0 || action == null)
            {
                return;
            }
            foreach (var item in list)
            {
                if (item == null) continue;
                action?.Invoke(item);
            }
        }

        public static bool DoSomethingForEachBreak<T>(this ICollection<T> list, Action<T> action, Func<T, bool> condition)
        {
            if (list == null || action == null || condition == null)
            {
                return false;
            }
            foreach (var item in list)
            {
                if (item == null) continue;
                bool chk = condition.Invoke(item);
                if (chk)
                {
                    action.Invoke(item);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// for async while list was modified
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="action"></param>
        public static ICollection<T> DoSomethingForEachInCopies<T>(this ICollection<T> list, Action<T> action)
        {
            if (list == null || list.Count == 0 || action == null)
            {
                return null;
            }
            var result = list.ToArray();
            foreach (var item in result)
            {
                if (item == null) continue;
                action?.Invoke(item);
            }
            //result = null;
            //GC.Collect();
            return result;
        }

        /// <summary>
        /// for async while list was modified
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="action"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static bool DoSomethingForEachBreakInCopies<T>(this ICollection<T> list, Action<T> action, Func<T, bool> condition)
        {
            if (list == null || action == null || condition == null)
            {
                return false;
            }
            var array = list.ToArray();
            foreach (var item in array)
            {
                if (item == null) continue;
                bool chk = condition.Invoke(item);
                if (chk)
                {
                    action.Invoke(item);
                    return true;
                }
            }
            return false;
        }

        //public static void AddRangeWithCapacity<T>(this ICollection<T> list, IEnumerable<T> collection, int capacity = 1024)
        //{
        //    if(list == null || collection == null)
        //    {
        //        return;
        //    }
        //    if(capacity <= 0 )
        //    {
        //        capacity = 1024;
        //    }
        //    try
        //    {
        //        list.AddRange(collection);
        //    }
        //    catch { }
        //    if(list.Count > capacity)
        //    {
        //        list.RemoveAt(0);
        //    }
        //}

        public static void RemoveAt<T>(this ICollection<T> list, int index)
        {
            if(list == null || index < 0 || index >= list.Count)
            {
                return;
            }
            T find;
            if(!list.GetElementAt(index, out find))
            {
                return;
            }
            try
            {
                list.Remove(find);
            }
            catch { }
        }

        public static bool GetElementAt<T>(this ICollection<T> list, int index, out T find)
        {
            find = default(T);
            if (list == null || index < 0 || index >= list.Count)
            {
                return false;
            }

            bool result = false;
            int cnt = 0;
            foreach (var item in list)
            {
                if (cnt != index) continue;
                find = item;
                result = true;
            }

            return result;
        }

        public static string ToPrintObject<T>(this ICollection<T> list)
        {
            if (list == null || list.Count==0)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            list.DoSomethingForEach((item) =>
            {
                sb.Append(item.ToString());
                sb.Append(", ");
            });

            return sb.ToString();
        }

        public static List<T> UpdateForEach<T>(this ICollection<T> list, Func<T,T> action)
        {
            if (list == null || list.Count == 0 || action == null)
            {
                return null;
            }
            List<T> result = new List<T>();
            foreach (var item in list)
            {
                if (item == null) continue;
                T newValue = action.Invoke(item);
                result.Add(newValue);
            }

            return result;
        }
    }
}
