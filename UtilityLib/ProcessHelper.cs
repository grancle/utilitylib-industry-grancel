﻿using LoggerLib;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;

namespace UtilityLib
{
    public class ProcessHelper
    {
        public static ProcessHelper Instance { get; } = new ProcessHelper();

        public Process FindProcess(string processName)
        {
            try
            {
                Process[] processes = Process.GetProcesses();

                foreach (Process item in processes)
                {
                    if (item.ProcessName == processName)
                    {
                        return item;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception in FindProcess" + "\t" + ex.ToString());
                Logger.Instance.Log(ex);
            }
            return null;
        }

        public IntPtr FindProcessMainWindowHandle(string processName)
        {
            Process _process = FindProcess(processName);
            if (_process == null) return IntPtr.Zero;
            return _process.MainWindowHandle;
        }

        public bool StartProcess(string name, string[] args = null, bool runas = false, bool IsWait = false)
        {
            bool result = false;
            try
            {
                result = StartProcessImpl(name, args, runas, IsWait);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(new Exception($"StartProcess {name} error!!!"));
                Logger.Instance.Log(ex);
            }
            return result;
        }

        private bool StartProcessImpl(string name, string[] args, bool runas, bool IsWait)
        {
            ProcessStartInfo sfi = new ProcessStartInfo();
            if (runas)
            {
                sfi.Verb = "runas";
                sfi.UseShellExecute = true;
            }
            else
            {
                sfi.Verb = "open";
                sfi.UseShellExecute = false;
            }
            sfi.FileName = name;

            string cmd = string.Empty;
            if(args != null && args.Length >0)
            {
                cmd = string.Join(" ", args);
            }
             
            sfi.Arguments = cmd;

            //try
            //{
            //    sfi.UserName = WindowsIdentity.GetCurrent().Name;
            //}
            //catch { }

            Logger.Instance.Log($"StartProcess runas={runas}, IsWait={IsWait}: {name} {cmd}");

            Process p = Process.Start(sfi);

            int retcode = 0;
            if (IsWait)
            {
                p.WaitForExit();
                retcode = p.ExitCode;
            }

            bool result = retcode == 0;
            return result;
        }

        public int SendMessage(string windowTitle, Win32API.WndMessage msg)
        {
            if (string.IsNullOrEmpty(windowTitle))
            {
                return -1;
            }
            IntPtr hwnd = Win32API.FindWindow(null, windowTitle);
            if (hwnd == IntPtr.Zero)
            {
                Logger.Instance.Log("hwnd is null.", LogEnum.Warnning);
                return -1;
            }

            int result = Win32API.SendMessage(hwnd, (int)msg, IntPtr.Zero, IntPtr.Zero);
            Logger.Instance.Log(string.Format("SendMessage to {2}: msg={0}, return:{1}.", (int)msg, result, windowTitle));
            return result;
        }

        public bool ShowWindow(string windowTitle, Win32API.CmdShow nCmdShow = Win32API.CmdShow.SW_SHOW)
        {
            if(string.IsNullOrEmpty(windowTitle))
            {
                return false;
            }
            IntPtr hwnd = Win32API.FindWindow(null, windowTitle);
            if (hwnd == IntPtr.Zero)
            {
                Logger.Instance.Log("hwnd is null.", LogEnum.Warnning);
                return false;
            }

            bool result = Win32API.ShowWindow(hwnd, (int)nCmdShow);
            Logger.Instance.Log(string.Format("ShowWindow for {2}: msg={0}, return:{1}.", (int)nCmdShow, result, windowTitle));
            return result;
        }


        public bool KillProcess(string processName, int milliseconds)
        {
            bool kill = KillProcessSub(processName);
            int wait_milliseconds = 100;
            int total = milliseconds / wait_milliseconds;
            int cnt = 0;
            while (!kill)
            {
                if(cnt > total)
                {
                    break;
                }
                Thread.Sleep(wait_milliseconds);
                kill = KillProcessSub(processName);
                cnt++;
            }
            return kill;

        }

        private bool KillProcessSub(string processName)
        {
            Process p = FindProcess(processName);
            if (p == null)
            {
                return false;
            }
            try
            {
                p.Close();
            }
            catch
            {}

            p = FindProcess(processName);
            if (p == null)
            {
                return true;
            }
            try
            {
                p.Kill();
            }
            catch { }

            p = FindProcess(processName);
            return p == null;
        }

        public IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;
        /// <summary>
        /// 以当前登录系统的用户角色权限启动指定的进程
        /// </summary>
        /// <param name="ChildProcName">指定的进程(全路径)</param>
        public void CreateProcessAsUser(string ChildProcName, bool isShowWindow)
        {
            Logger.InstanceForService.Log($"CreateProcessAsUser begin. {ChildProcName}");
            IntPtr ppSessionInfo = IntPtr.Zero;
            UInt32 SessionCount = 0;
            if (Win32API.WTSEnumerateSessions(
                                    (IntPtr)WTS_CURRENT_SERVER_HANDLE,  // Current RD Session Host Server handle would be zero. 
                                    0,  // This reserved parameter must be zero. 
                                    1,  // The version of the enumeration request must be 1. 
                                    ref ppSessionInfo, // This would point to an array of session info. 
                                    ref SessionCount  // This would indicate the length of the above array.
                                    ))
            {
                for (int nCount = 0; nCount < SessionCount; nCount++)
                {
                    Win32API.WTS_SESSION_INFO tSessionInfo = (Win32API.WTS_SESSION_INFO)Marshal.PtrToStructure(ppSessionInfo + nCount * Marshal.SizeOf(typeof(Win32API.WTS_SESSION_INFO)), typeof(Win32API.WTS_SESSION_INFO));
                    if (Win32API.WTS_CONNECTSTATE_CLASS.WTSActive == tSessionInfo.State)
                    {
                        Logger.InstanceForService.Log($"get session={tSessionInfo.SessionID}");
                        IntPtr hToken = IntPtr.Zero;
                        if (Win32API.WTSQueryUserToken(tSessionInfo.SessionID, out hToken))
                        {
                            Logger.InstanceForService.Log($"get token={hToken.ToInt32()}");
                            Win32API.PROCESS_INFORMATION tProcessInfo;
                            Win32API.STARTUPINFO tStartUpInfo = new Win32API.STARTUPINFO();
                            tStartUpInfo.cb = isShowWindow ? Marshal.SizeOf(typeof(Win32API.STARTUPINFO)) : 0;
                            tStartUpInfo.wShowWindow = isShowWindow ? (short)Win32API.CmdShow.SW_SHOW : (short)Win32API.CmdShow.SW_HIDE;
                            tStartUpInfo.dwFlags = isShowWindow ? (int)Win32API.StartfFlags.STARTF_USESHOWWINDOW : (int)Win32API.StartfFlags.STARTF_FORCEONFEEDBACK;
                            bool ChildProcStarted = Win32API.CreateProcessAsUser(
                                                                        hToken,             // Token of the logged-on user. 
                                                                        ChildProcName,      // Name of the process to be started. 
                                                                        null,               // Any command line arguments to be passed. 
                                                                        IntPtr.Zero,        // Default Process' attributes. 
                                                                        IntPtr.Zero,        // Default Thread's attributes. 
                                                                        false,              // Does NOT inherit parent's handles. 
                                                                        0,                  // No any specific creation flag. 
                                                                        null,               // Default environment path. 
                                                                        null,               // Default current directory. 
                                                                        ref tStartUpInfo,   // Process Startup Info.  
                                                                        out tProcessInfo    // Process information to be returned. 
                                                     );
                            if (ChildProcStarted)
                            {
                                Win32API.CloseHandle(tProcessInfo.hThread);
                                Win32API.CloseHandle(tProcessInfo.hProcess);
                            }
                            else
                            {
                                uint ex = Win32API.GetLastError();
                                Logger.InstanceForService.Log(new Exception($"CreateProcessAsUser失败:{ex}"));
                                //Win32API.ShowServiceMessage("CreateProcessAsUser失败", "CreateProcess", WTS_CURRENT_SERVER_HANDLE);
                            }
                            Win32API.CloseHandle(hToken);
                            break;
                        }
                    }
                }
                Win32API.WTSFreeMemory(ppSessionInfo);
            }
        }
    

    }
}
