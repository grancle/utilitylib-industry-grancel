﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public delegate byte[] delCalChkCode(byte[] src);
    public class MacroStrHelper
    {
        public MacroStrHelper(string SrcValue)
        {
            this.SrcValue = SrcValue;
        }

        public string SrcValue { get; private set; }

        public List<MacroInfo> MyInfoArray { get; private set; } = new List<MacroInfo>();

        public int Count { get; private set; }

        public bool IsValid { get; private set; } = false;

        public void Convert()
        {
            try
            {
                ConvertImpl();
            }
            catch { }
        }

        private void ConvertImpl()
        {
            if(string.IsNullOrEmpty(SrcValue))
            {
                return;
            }

            char[] array = SrcValue.ToArray();

            StringBuilder sb = null;
            foreach(char item in array)
            {
                if (_is_need_create_new)
                {
                    sb = new StringBuilder();
                    _is_need_create_new = false;
                }

                if (ChkBeginChar(item, sb)) continue;

                if (WriteSymbol(item)) continue;

                sb.Append(item);
            }

            if (_is_need_create_new)
            {
                sb = new StringBuilder();
                _is_need_create_new = false;
            }

            if (!_is_write_prop_on && !_is_write_lable_on && sb.Length > 0)
            {
                _is_write_lable_on = true;
            }
            Write(sb);

            MyInfoArray.Sort((x, y) =>
            {
                return x.Index - y.Index;
            });

            Count = _index;

            IsValid = true;
        }

        private int _index = 0;
        private bool _is_need_create_new = true;
        private bool _is_write_prop_on = false;
        private bool _is_write_lable_on = false;
        private bool _is_write_sysmbol_on = false;

        private bool ChkBeginChar(char c, StringBuilder sb)
        {
            switch(c)
            {
                case '}':
                    _is_write_prop_on = true;
                    _is_write_lable_on = false;
                    Write(sb);
                    _is_write_prop_on = false;
                    _is_need_create_new = true;
                    return true;
                case '{':
                    _is_write_prop_on = false;
                    _is_write_lable_on = true;
                    Write(sb);
                    _is_write_lable_on = false;
                    _is_need_create_new = true;
                    return true;
                case '+':
                case '-':
                case '*':
                case '/':
                case '=':
                    _is_write_lable_on = true;
                    _is_write_sysmbol_on = true;
                    return false;
                default:
                    return false;
            }

        }

        private void Write(StringBuilder sb)
        {
            if(sb == null || sb.Length == 0)
            {
                return;
            }
            if (_is_write_prop_on)
            {
                MyInfoArray.Add(new MacroInfo(_index, sb.ToString(), null));
                _is_write_prop_on = false;
                _index++;
                return;
            }
            if (_is_write_lable_on)
            {
                MyInfoArray.Add(new MacroInfo(_index, sb.ToString()));
                _is_write_lable_on = false;
                _index++;
                return;
            }
        }

        private bool WriteSymbol(char c)
        {
            if(!_is_write_sysmbol_on)
            {
                return false;
            }
            switch (c)
            {
                case '+':
                case '-':
                case '*':
                case '/':
                case '=':
                    MyInfoArray.Add(new MacroInfo(_index, c));
                    _index++;
                    break;
                default:
                    return false;
            }

            _is_need_create_new = true;
            return true;
        }

        public string GetValue(params MacroInfo[] infos)
        {
            string result = string.Empty;
            try
            {
                result = GetValueImpl(infos);
            }
            catch { }
            return result;
        }

        private string GetValueImpl(MacroInfo[] infos)
        {
            if(MyInfoArray == null || MyInfoArray.Count == 0)
            {
                return SrcValue;
            }

            StringBuilder sb = new StringBuilder();
            MyInfoArray.DoSomethingForEach((item) =>
            {
                sb.Append(GetValueImpl(item, infos));
            });

            return sb.ToString();
        }

        private string GetValueImpl(MacroInfo item, MacroInfo[] infos)
        {
            if(item == null)
            {
                return string.Empty;
            }
            if (infos == null || infos.Length == 0)
            {
                return item.ToString();
            }
            if(string.IsNullOrEmpty(item.Name))
            {
                return item.ToString();
            }

            string result = string.Empty;
            infos.DoSomethingForEachBreak((p) =>
            {
                result = p.ToString();
            },(p)=> {
                return p.Name == item.Name;
            });

            return result;
        }

        public int GetInt(params MacroInfo[] infos)
        {
            int result = 0;
            try
            {
                result = GetIntImpl(infos);
            }
            catch { }
            return result;
        }

        private int GetIntImpl(MacroInfo[] infos)
        {
            if (MyInfoArray == null || MyInfoArray.Count == 0)
            {
                return 0;
            }

            int result = 0;
            char sysmbol = '+';
            MyInfoArray.DoSomethingForEach((item) =>
            {
                if(item.Value.GetType() == typeof(char))
                {
                    sysmbol = item.Value.ConvertHard<char>();
                    return;
                }
                int val = GetIntImpl(item, infos);
                switch (sysmbol)
                {
                    case '-':
                        result -= val;
                        break;
                    case '*':
                    case 'x':
                        result *= val;
                        break;
                    case '/':
                        if (val == 0) return;
                        result /= val;
                        break;
                    case '%':
                        result %= val;
                        break;
                    case '+':
                    default:
                        result += val;
                        break;
                }

            });

            return result;
        }

        private int GetIntImpl(MacroInfo item, MacroInfo[] infos)
        {
            if (item == null)
            {
                return 0;
            }

            if (infos == null || infos.Length == 0)
            {
                return item.ToInt();
            }

            if (string.IsNullOrEmpty(item.Name))
            {
                return item.ToInt();
            }

            int result = 0;
            infos.DoSomethingForEachBreak((p) =>
            {
                result = p.ToInt();
            }, (p) => {
                return p.Name == item.Name;
            });

            return result;
        }

        public byte[] GetByte(delCalChkCode cal_chk_func, params MacroInfo[] infos)
        {
            byte[] result = null;
            try
            {
                result = GetByteImpl(cal_chk_func, infos);
            }
            catch { }
            return result;
        }

        private byte[] GetByteImpl(delCalChkCode cal_chk_func, MacroInfo[] infos)
        {
            if (MyInfoArray == null || MyInfoArray.Count == 0)
            {
                return null;
            }

            List<byte> result = new List<byte>();
            int index_chk = 0;
            MyInfoArray.DoSomethingForEach((item) =>
            {
                if(item.Name == NAME_CHK)
                {
                    index_chk = result.Count;
                    return;
                }
                byte[] val = GetByteImpl(item, infos);
                if(val == null)
                {
                    return;
                }
                result.AddRange(val);
            });

            byte[] chk = cal_chk_func?.Invoke(result.ToArray());
            if(chk != null && chk.Length >0)
            {
                result.InsertRange(index_chk, chk);
            }

            return result.ToArray();
        }

        private byte[] GetByteImpl(MacroInfo item, MacroInfo[] infos)
        {
            if (item == null)
            {
                return null;
            }

            if (infos == null || infos.Length == 0)
            {
                return item.ToByte();
            }

            if (string.IsNullOrEmpty(item.Name))
            {
                return item.ToByte();
            }

            byte[] result = null;
            infos.DoSomethingForEachBreak((p) =>
            {
                result = p.ToByte();
            }, (p) => {
                return p.Name == item.Name;
            });

            return result;
        }

        private const string NAME_CHK = "chk";

        public byte[] GetByte(delCalChkCode cal_chk_func, byte[] data, byte[] prefix, byte[] suffix, byte[] middle = null)
        {
            return GetByte(cal_chk_func, new MacroInfo(nameof(data), data),
                                             new MacroInfo(nameof(prefix), prefix),
                                             new MacroInfo(nameof(suffix), suffix),
                                             new MacroInfo(nameof(middle), middle));
        }

    }
}
