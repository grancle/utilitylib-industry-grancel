﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class ConverterHelper
    {
        public static T ConvertHard<T>(this object value)
        {
            T result = default(T);
            if(value == null)
            {
                return result;
            }
            try
            {
                result = (T)value;
            }
            catch { }
            return result;
        }
    }
}
