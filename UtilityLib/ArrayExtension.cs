﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class ArrayExtension
    {
        public static void CopyTo<T>(this T[] array, int index, int count, out T[] target)
        {
            target = null;
            if(array == null ||array.Length ==0 || 
                    index<0 || count<=0)
            {
                return;
            }

            List<T> result = new List<T>();
            //copy = new T[count];
            int j = 0;
            for(int i=0;i<array.Length;i++)
            {
                if(i< index)
                {
                    continue;
                }
                result.Add(array[i]);
                //target[j] = array[i];
                j++;
                if(j == count)
                {
                    break;
                }
            }

            target = result.ToArray();
        }

        public static T[] Insert<T>(this T[] array, int index, T value)
        {
            if(array == null ||array.Length ==0)
            {
                return new T[] { value };
            }

            T[] result = new T[array.Length + 1];
            if (index >= array.Length)
            {
                Array.Copy(array, 0, result, 0, array.Length);
                result[array.Length-1] = value;
                return result;
            }

            if(index > 0)
            {
                Array.Copy(array, 0, result, 0, index);
            }
    
            Array.Copy(array, index, result, index + 1, array.Length - index);
            result[index] = value;

            return result;
            //List<T> list = array == null ? new List<T>() : array.ToList();

            //list.Insert(index, value);

            //return list.ToArray();  
        }
    }
}
