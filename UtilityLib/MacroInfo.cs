﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class MacroInfo
    {
        public MacroInfo(int index, string Name, object Value)
        {
            this.Index = index;
            this.Name = Name;
            this.Value = Value;
        }

        //public MacroInfo(int index, string name, MacroEnum type)
        //{
        //    this.Index = index;
        //    this.Name = name;
        //    this.type = type;
        //}

        public MacroInfo(int index, object Value)
        {
            this.Index = index;
            this.Value = Value;
        }

        //public MacroInfo(int index, char sysmbol)
        //{
        //    this.Index = index;
        //    this.MySysmbol = sysmbol;
        //    this.type = MacroEnum.Sysmbol;
        //}

        public MacroInfo(string Name, object Value)
        {
            this.Name = Name;
            this.Value = Value;
        }

        //public MacroEnum type { get; set; } = MacroEnum.Lable;

        public int Index { get; set; }
        public string Name { get; set; }

        public object Value { get; set; }

        //public char MySysmbol { get; set; }

        public delegate string delToString(object value);

        public delToString MyFuncToString { get; set; }

        public override string ToString()
        {
            if(MyFuncToString == null)
            {
                return Value?.ToString();
            }

            return MyFuncToString(Value);
        }

        public int ToInt()
        {
            if(Value == null || typeof(int) != Value.GetType())
            {
                return 0;
            }

            return Value.ConvertHard<int>();
        }

        public byte[] ToByte()
        {
            if (Value ==null || typeof(byte[]) != Value.GetType())
            {
                return null;
            }

            return Value.ConvertHard<byte[]>();
        }
    }

    //public enum MacroEnum
    //{
    //    Lable = 0,
    //    Prop,
    //    Sysmbol,
    //}
}
