﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class Formulars
    {
        public static Formulars Instance { get; } = new Formulars();

        public int ConvertStrToInt(string src)
        {
            int result = 0;
            try
            {
                result = Convert.ToInt32(src);
            }
            catch { }

            return result;
        }

        public string ConvertByteToStr(byte[] src, Encoding encoding)
        {
            if (src == null || src.Length <= 0 || encoding == null)
            {
                return string.Empty;
            }
            string result = string.Empty;
            try
            {
                byte[] target = TailZeroByte(src);
                result = encoding.GetString(target);
                //result = encoding.GetString(src);
            }
            catch { }
            return result;
        }

        public byte[] TailZeroByte(byte[] src)
        {
            if (src == null || src.Length <= 0)
            {
                return null;
            }
            List<byte> result = new List<byte>();
            for (int i = 0; i < src.Length; i++)
            {
                if (src[i] == '\0')
                {
                    break;
                }
                result.Add(src[i]);
            }

            return result.ToArray();
        }

        public int ConvertIntNullToInt(int? src, int initial_value = 0)
        {
            int result = initial_value;
            if(src != null && src > initial_value)
            {
                result = (int)src;
            }
            return result;
        }

        public bool ConvertBoolNullToBool(bool? src, bool initial_value = false)
        {
            bool result = initial_value;
            if (src != null)
            {
                result = src.Value;
            }
            return result;
        }

        public int ConvertBoolNullToInt(bool? src)
        {
            int result = 0;
            if (src != null)
            {
                result = src.Value ? 1 : 0;
            }
            return result;
        }

        public double ConvertStrToDouble(string src)
        {
            double result = 0;
            try
            {
                result = Convert.ToDouble(src);
            }
            catch { }

            return result;
        }

        public bool AreEqualDouble(double left, double right, int digit = 2)
        {
            bool result = false;
            try
            {
                result = AreEqualDoubleImpl(left, right, digit);
            }
            catch { }

            return result;

        }

        private bool AreEqualDoubleImpl(double left, double right, int digit)
        {
            int multiple = (int)Math.Pow(10, 2);
            int left_int = (int)(left * multiple);
            int right_int = (int)(right * multiple);
            bool result = left_int == right_int;
            return result;
        }

        public void SetDoubleIfNotEqual(ref double left, double right, int digit = 2)
        {
            if(AreEqualDouble(left, right, digit))
            {
                return;
            }
            left = right;
        }

        public decimal ConvertStrToDecimal(string src)
        {
            decimal result = 0;
            try
            {
                result = Convert.ToDecimal(src);
            }
            catch { }

            return result;
        }

        public bool ConvertStrToBool(string src, bool initial_value = false)
        {
            bool result = initial_value;
            if(string.IsNullOrEmpty(src))
            {
                return result;
            }
            switch(src.ToLower())
            {
                case "true":
                    result = true;
                    break;
                case "false":
                    result = false;
                    break;
                default:
                    break;
            }

            return result;

        }

        public byte[] ConvertStrToByte(string src, Encoding encoding)
        {
            if(string.IsNullOrEmpty(src))
            {
                return null;
            }

            byte[] result = null;

            try
            {
                result = encoding.GetBytes(src);
            }
            catch { }

            return result;
        }

        public void CheckValue(ref int val, int min, int max)
        {
            if(val < min)
            {
                val = min;
            }
            if(val > max)
            {
                val = max;
            }
        }

        public string CutDoubleZeroTail(string src)
        {
            string result = src;
            if (result.Contains('.') && result.EndsWith("0"))
            {
                while (result.EndsWith("0"))
                {
                    result = result.Substring(0, result.Length - 1);
                    if (result.EndsWith("."))
                    {
                        result = result.Substring(0, result.Length - 1);
                        break;
                    }
                }
            }
            return result;
        }

        public string PrintByte(byte[] src)
        {
            if (src == null || src.Length <= 0)
            {
                return string.Empty;
            }
            string result = string.Empty;
            foreach (byte item in src)
            {
                result += item.ToString("X2") + " ";
            }
            return result;
        }

        public byte[] ConvertIntToX16(int src)
        {
            List<byte> result = new List<byte>();
            try
            {
                while (true)
                {
                    int remainder = src % 256;
                    byte buff = ConvertIntToX16Single(remainder);
                    result.Add(buff);
                    int quotient = src / 256;
                    if (quotient == 0) break;
                    src = quotient;
                }
                result.Reverse();
            }
            catch { }

            return result.ToArray();
        }

        public byte[] InvertCode(byte[] src)
        {

            try
            {
                return InvertCodeImpl(src);
            }
            catch { }

            return null;
        }

        public byte[] InvertCodeImpl(byte[] src)
        {
            if(src == null || src.Length == 0)
            {
                return null;
            }

            List<byte> result = new List<byte>();
            foreach(byte item in src)
            {
                byte val = (byte)~item;
                result.Add(val);
            }

            return result.ToArray();
        }

        public byte[] ComplementCode(byte[] src, int code = 1)
        {
            try
            {
                return ComplementCodeImpl(src, code);
            }
            catch { }

            return null;
        }

        private byte[] ComplementCodeImpl(byte[] src, int code)
        {
            if (src == null || src.Length == 0)
            {
                return null;
            }

            byte[] result = new byte[src.Length];
            int is_overload = 0;
            result[src.Length - 1] = ComplementCodeImpl(src[src.Length - 1], code, out is_overload);
            int index = src.Length - 2;
            while (is_overload !=0 && index>=0)
            {
                result[index] = ComplementCodeImpl(src[index], is_overload, out is_overload);
                index--;
            }
            
            return result;
        }

        private byte ComplementCodeImpl(byte src, int code, out int is_overload)
        {
            is_overload = 0;
            int result = src + code;
            if(result <0)
            {
                is_overload = -1;
            }
            else if(result > 255)
            {
                is_overload = 1;
            }

            return (byte)result;
        }

        public byte[] PaddingByte(byte[] src, int length, bool is_get_last = true)
        {
            if (src == null || src.Length == 0)
            {
                return new byte[length];
            }

            int diff = src.Length - length;
            int j = 0;

            byte[] result = new byte[length];
            if(diff >= 0)
            {
                for(int i=0;i<length;i++)
                {
                    j = is_get_last ? diff + i : i;
                    result[i] = src[j];
                }
                return result;
            }

            //int j = 0;
            diff = length - src.Length;
            for (int i = 0; i < length; i++)
            {
                bool is_padding = i < diff;
                if(is_padding)
                {
                    result[i] = 0x00;
                    continue;
                }
                result[i] = src[j];
                j++;
            }

            return result;
        }

        public byte[] GetMiddleBytesFromSrc(byte[] src, int index, int count)
        {
            if (count <= 0)
            {
                return null;
            }
            byte[] result = new byte[count];
            try
            {
                int j = 0;
                for (int i = 0; i < src.Length; i++)
                {
                    if (i < index)
                    {
                        continue;
                    }
                    result[j] = src[i];
                    j++;
                    if (j >= count)
                    {
                        break;
                    }
                }
                //int len = src.Length - index;
                //byte[] last = new byte[len];

                //src.CopyTo(last, index);

                //last.CopyTo(result, 0);
            }
            catch { }

            return result;
        }

        public byte[] GetLastBytesFromSrc(byte[] src, int count)
        {
            if(count <=0)
            {
                return null;
            }
            byte[] result = new byte[count];
            try
            {
                src.CopyTo(result, src.Length - count);
            }
            catch { }
            return result;
        }

        public byte GetHighDigit(byte[] src)
        {
            if(src == null || src.Length == 0)
            {
                return 0x00;
            }

            byte result = 0x00;
            try
            {
                result = (byte)(src[0] >> 7);
            }
            catch { }
   
            return result;
        }

        public int ConvertByteToInt(byte[] src)
        {
            if(src == null || src.Length == 0)
            {
                return 0;
            }

            int result = 0;
            int j = 0;
            for(int i= src.Length -1;i>=0;i--)
            {
                result += (int)(src[i] * Math.Pow(256, j));
                j++;
            }
            return result;
        }

        private byte ConvertIntToX16Single(int src)
        {
            byte result = 0x00;
            try
            {
                switch(src)
                {
                    case 0:
                        result = 0x00;
                        break;
                    case 1:
                        result = 0x01;
                        break;
                    case 2:
                        result = 0x02;
                        break;
                    case 3:
                        result = 0x03;
                        break;
                    case 4:
                        result = 0x04;
                        break;
                    case 5:
                        result = 0x05;
                        break;
                    case 6:
                        result = 0x06;
                        break;
                    case 7:
                        result = 0x07;
                        break;
                    case 8:
                        result = 0x08;
                        break;
                    case 9:
                        result = 0x09;
                        break;
                    case 10:
                        result = 0x0A;
                        break;
                    case 11:
                        result = 0x0B;
                        break;
                    case 12:
                        result = 0x0C;
                        break;
                    case 13:
                        result = 0x0D;
                        break;
                    case 14:
                        result = 0x0E;
                        break;
                    case 15:
                        result = 0x0F;
                        break;
                    case 16:
                        result = 0x10;
                        break;
                    case 17:
                        result = 0x11;
                        break;
                    case 18:
                        result = 0x12;
                        break;
                    case 19:
                        result = 0x13;
                        break;
                    case 20:
                        result = 0x14;
                        break;
                    case 21:
                        result = 0x15;
                        break;
                    case 22:
                        result = 0x16;
                        break;
                    case 23:
                        result = 0x17;
                        break;
                    case 24:
                        result = 0x18;
                        break;
                    case 25:
                        result = 0x19;
                        break;
                    case 26:
                        result = 0x1A;
                        break;
                    case 27:
                        result = 0x1B;
                        break;
                    case 28:
                        result = 0x1C;
                        break;
                    case 29:
                        result = 0x1D;
                        break;
                    case 30:
                        result = 0x1E;
                        break;
                    case 31:
                        result = 0x1F;
                        break;
                    case 32:
                        result = 0x20;
                        break;
                    case 33:
                        result = 0x21;
                        break;
                    case 34:
                        result = 0x22;
                        break;
                    case 35:
                        result = 0x23;
                        break;
                    case 36:
                        result = 0x24;
                        break;
                    case 37:
                        result = 0x25;
                        break;
                    case 38:
                        result = 0x26;
                        break;
                    case 39:
                        result = 0x27;
                        break;
                    case 40:
                        result = 0x28;
                        break;
                    case 41:
                        result = 0x29;
                        break;
                    case 42:
                        result = 0x2A;
                        break;
                    case 43:
                        result = 0x2B;
                        break;
                    case 44:
                        result = 0x2C;
                        break;
                    case 45:
                        result = 0x2D;
                        break;
                    case 46:
                        result = 0x2E;
                        break;
                    case 47:
                        result = 0x2F;
                        break;
                    case 48:
                        result = 0x30;
                        break;
                    case 49:
                        result = 0x31;
                        break;
                    case 50:
                        result = 0x32;
                        break;
                    case 51:
                        result = 0x33;
                        break;
                    case 52:
                        result = 0x34;
                        break;
                    case 53:
                        result = 0x35;
                        break;
                    case 54:
                        result = 0x36;
                        break;
                    case 55:
                        result = 0x37;
                        break;
                    case 56:
                        result = 0x38;
                        break;
                    case 57:
                        result = 0x39;
                        break;
                    case 58:
                        result = 0x3A;
                        break;
                    case 59:
                        result = 0x3B;
                        break;
                    case 60:
                        result = 0x3C;
                        break;
                    case 61:
                        result = 0x3D;
                        break;
                    case 62:
                        result = 0x3E;
                        break;
                    case 63:
                        result = 0x3F;
                        break;
                    case 64:
                        result = 0x40;
                        break;
                    case 65:
                        result = 0x41;
                        break;
                    case 66:
                        result = 0x42;
                        break;
                    case 67:
                        result = 0x43;
                        break;
                    case 68:
                        result = 0x44;
                        break;
                    case 69:
                        result = 0x45;
                        break;
                    case 70:
                        result = 0x46;
                        break;
                    case 71:
                        result = 0x47;
                        break;
                    case 72:
                        result = 0x48;
                        break;
                    case 73:
                        result = 0x49;
                        break;
                    case 74:
                        result = 0x4A;
                        break;
                    case 75:
                        result = 0x4B;
                        break;
                    case 76:
                        result = 0x4C;
                        break;
                    case 77:
                        result = 0x4D;
                        break;
                    case 78:
                        result = 0x4E;
                        break;
                    case 79:
                        result = 0x4F;
                        break;
                    case 80:
                        result = 0x50;
                        break;
                    case 81:
                        result = 0x51;
                        break;
                    case 82:
                        result = 0x52;
                        break;
                    case 83:
                        result = 0x53;
                        break;
                    case 84:
                        result = 0x54;
                        break;
                    case 85:
                        result = 0x55;
                        break;
                    case 86:
                        result = 0x56;
                        break;
                    case 87:
                        result = 0x57;
                        break;
                    case 88:
                        result = 0x58;
                        break;
                    case 89:
                        result = 0x59;
                        break;
                    case 90:
                        result = 0x5A;
                        break;
                    case 91:
                        result = 0x5B;
                        break;
                    case 92:
                        result = 0x5C;
                        break;
                    case 93:
                        result = 0x5D;
                        break;
                    case 94:
                        result = 0x5E;
                        break;
                    case 95:
                        result = 0x5F;
                        break;
                    case 96:
                        result = 0x60;
                        break;
                    case 97:
                        result = 0x61;
                        break;
                    case 98:
                        result = 0x62;
                        break;
                    case 99:
                        result = 0x63;
                        break;
                    case 100:
                        result = 0x64;
                        break;
                    case 101:
                        result = 0x65;
                        break;
                    case 102:
                        result = 0x66;
                        break;
                    case 103:
                        result = 0x67;
                        break;
                    case 104:
                        result = 0x68;
                        break;
                    case 105:
                        result = 0x69;
                        break;
                    case 106:
                        result = 0x6A;
                        break;
                    case 107:
                        result = 0x6B;
                        break;
                    case 108:
                        result = 0x6C;
                        break;
                    case 109:
                        result = 0x6D;
                        break;
                    case 110:
                        result = 0x6E;
                        break;
                    case 111:
                        result = 0x6F;
                        break;
                    case 112:
                        result = 0x70;
                        break;
                    case 113:
                        result = 0x71;
                        break;
                    case 114:
                        result = 0x72;
                        break;
                    case 115:
                        result = 0x73;
                        break;
                    case 116:
                        result = 0x74;
                        break;
                    case 117:
                        result = 0x75;
                        break;
                    case 118:
                        result = 0x76;
                        break;
                    case 119:
                        result = 0x77;
                        break;
                    case 120:
                        result = 0x78;
                        break;
                    case 121:
                        result = 0x79;
                        break;
                    case 122:
                        result = 0x7A;
                        break;
                    case 123:
                        result = 0x7B;
                        break;
                    case 124:
                        result = 0x7C;
                        break;
                    case 125:
                        result = 0x7D;
                        break;
                    case 126:
                        result = 0x7E;
                        break;
                    case 127:
                        result = 0x7F;
                        break;
                    case 128:
                        result = 0x80;
                        break;
                    case 129:
                        result = 0x81;
                        break;
                    case 130:
                        result = 0x82;
                        break;
                    case 131:
                        result = 0x83;
                        break;
                    case 132:
                        result = 0x84;
                        break;
                    case 133:
                        result = 0x85;
                        break;
                    case 134:
                        result = 0x86;
                        break;
                    case 135:
                        result = 0x87;
                        break;
                    case 136:
                        result = 0x88;
                        break;
                    case 137:
                        result = 0x89;
                        break;
                    case 138:
                        result = 0x8A;
                        break;
                    case 139:
                        result = 0x8B;
                        break;
                    case 140:
                        result = 0x8C;
                        break;
                    case 141:
                        result = 0x8D;
                        break;
                    case 142:
                        result = 0x8E;
                        break;
                    case 143:
                        result = 0x8F;
                        break;
                    case 144:
                        result = 0x90;
                        break;
                    case 145:
                        result = 0x91;
                        break;
                    case 146:
                        result = 0x92;
                        break;
                    case 147:
                        result = 0x93;
                        break;
                    case 148:
                        result = 0x94;
                        break;
                    case 149:
                        result = 0x95;
                        break;
                    case 150:
                        result = 0x96;
                        break;
                    case 151:
                        result = 0x97;
                        break;
                    case 152:
                        result = 0x98;
                        break;
                    case 153:
                        result = 0x99;
                        break;
                    case 154:
                        result = 0x9A;
                        break;
                    case 155:
                        result = 0x9B;
                        break;
                    case 156:
                        result = 0x9C;
                        break;
                    case 157:
                        result = 0x9D;
                        break;
                    case 158:
                        result = 0x9E;
                        break;
                    case 159:
                        result = 0x9F;
                        break;
                    case 160:
                        result = 0xA0;
                        break;
                    case 161:
                        result = 0xA1;
                        break;
                    case 162:
                        result = 0xA2;
                        break;
                    case 163:
                        result = 0xA3;
                        break;
                    case 164:
                        result = 0xA4;
                        break;
                    case 165:
                        result = 0xA5;
                        break;
                    case 166:
                        result = 0xA6;
                        break;
                    case 167:
                        result = 0xA7;
                        break;
                    case 168:
                        result = 0xA8;
                        break;
                    case 169:
                        result = 0xA9;
                        break;
                    case 170:
                        result = 0xAA;
                        break;
                    case 171:
                        result = 0xAB;
                        break;
                    case 172:
                        result = 0xAC;
                        break;
                    case 173:
                        result = 0xAD;
                        break;
                    case 174:
                        result = 0xAE;
                        break;
                    case 175:
                        result = 0xAF;
                        break;
                    case 176:
                        result = 0xB0;
                        break;
                    case 177:
                        result = 0xB1;
                        break;
                    case 178:
                        result = 0xB2;
                        break;
                    case 179:
                        result = 0xB3;
                        break;
                    case 180:
                        result = 0xB4;
                        break;
                    case 181:
                        result = 0xB5;
                        break;
                    case 182:
                        result = 0xB6;
                        break;
                    case 183:
                        result = 0xB7;
                        break;
                    case 184:
                        result = 0xB8;
                        break;
                    case 185:
                        result = 0xB9;
                        break;
                    case 186:
                        result = 0xBA;
                        break;
                    case 187:
                        result = 0xBB;
                        break;
                    case 188:
                        result = 0xBC;
                        break;
                    case 189:
                        result = 0xBD;
                        break;
                    case 190:
                        result = 0xBE;
                        break;
                    case 191:
                        result = 0xBF;
                        break;
                    case 192:
                        result = 0xC0;
                        break;
                    case 193:
                        result = 0xC1;
                        break;
                    case 194:
                        result = 0xC2;
                        break;
                    case 195:
                        result = 0xC3;
                        break;
                    case 196:
                        result = 0xC4;
                        break;
                    case 197:
                        result = 0xC5;
                        break;
                    case 198:
                        result = 0xC6;
                        break;
                    case 199:
                        result = 0xC7;
                        break;
                    case 200:
                        result = 0xC8;
                        break;
                    case 201:
                        result = 0xC9;
                        break;
                    case 202:
                        result = 0xCA;
                        break;
                    case 203:
                        result = 0xCB;
                        break;
                    case 204:
                        result = 0xCC;
                        break;
                    case 205:
                        result = 0xCD;
                        break;
                    case 206:
                        result = 0xCE;
                        break;
                    case 207:
                        result = 0xCF;
                        break;
                    case 208:
                        result = 0xD0;
                        break;
                    case 209:
                        result = 0xD1;
                        break;
                    case 210:
                        result = 0xD2;
                        break;
                    case 211:
                        result = 0xD3;
                        break;
                    case 212:
                        result = 0xD4;
                        break;
                    case 213:
                        result = 0xD5;
                        break;
                    case 214:
                        result = 0xD6;
                        break;
                    case 215:
                        result = 0xD7;
                        break;
                    case 216:
                        result = 0xD8;
                        break;
                    case 217:
                        result = 0xD9;
                        break;
                    case 218:
                        result = 0xDA;
                        break;
                    case 219:
                        result = 0xDB;
                        break;
                    case 220:
                        result = 0xDC;
                        break;
                    case 221:
                        result = 0xDD;
                        break;
                    case 222:
                        result = 0xDE;
                        break;
                    case 223:
                        result = 0xDF;
                        break;
                    case 224:
                        result = 0xE0;
                        break;
                    case 225:
                        result = 0xE1;
                        break;
                    case 226:
                        result = 0xE2;
                        break;
                    case 227:
                        result = 0xE3;
                        break;
                    case 228:
                        result = 0xE4;
                        break;
                    case 229:
                        result = 0xE5;
                        break;
                    case 230:
                        result = 0xE6;
                        break;
                    case 231:
                        result = 0xE7;
                        break;
                    case 232:
                        result = 0xE8;
                        break;
                    case 233:
                        result = 0xE9;
                        break;
                    case 234:
                        result = 0xEA;
                        break;
                    case 235:
                        result = 0xEB;
                        break;
                    case 236:
                        result = 0xEC;
                        break;
                    case 237:
                        result = 0xED;
                        break;
                    case 238:
                        result = 0xEE;
                        break;
                    case 239:
                        result = 0xEF;
                        break;
                    case 240:
                        result = 0xF0;
                        break;
                    case 241:
                        result = 0xF1;
                        break;
                    case 242:
                        result = 0xF2;
                        break;
                    case 243:
                        result = 0xF3;
                        break;
                    case 244:
                        result = 0xF4;
                        break;
                    case 245:
                        result = 0xF5;
                        break;
                    case 246:
                        result = 0xF6;
                        break;
                    case 247:
                        result = 0xF7;
                        break;
                    case 248:
                        result = 0xF8;
                        break;
                    case 249:
                        result = 0xF9;
                        break;
                    case 250:
                        result = 0xFA;
                        break;
                    case 251:
                        result = 0xFB;
                        break;
                    case 252:
                        result = 0xFC;
                        break;
                    case 253:
                        result = 0xFD;
                        break;
                    case 254:
                        result = 0xFE;
                        break;
                    case 255:
                        result = 0xFF;
                        break;
                    //case 0:
                    default:
                        break;
                }
            }
            catch { }
            return result;
        }
    }
}
