﻿using System;
using System.Collections.Generic;

namespace UtilityLib
{
    public class ByteConverter
    {
        public static ByteConverter Instance { get; } = new ByteConverter();

        public char[] ConvertByteToCharArray(byte[] b)
        {
            if (b == null || b.Length ==0)
            {
                return null;
            }
            if (b.Length == 1)
            {
                b = new byte[] { 0x00, b[0] };
            }
            List<char> result = new List<char>();
            for(int i=0;i<b.Length;i+=2)
            {
                char c = ConvertByteToChar(new byte[] { b[i], b[i + 1] });
                result.Add(c);
            }
            return result.ToArray();
        }

        public char ConvertByteToChar(byte[] b)
        {
            if(b == null || b.Length ==0)
            {
                return (char)0x0000;
            }
            if(b.Length == 1)
            {
                b = new byte[] { 0x00, b[0] };
            }
            char c = (char)(((b[0] & 0xFF) << 8) | (b[1] & 0xFF));
            return c;
        }

        public byte[] ConvertCharArrayToByte(char[] c)
        {
            if(c == null || c.Length ==0)
            {
                return null;
            }
            List<byte> result = new List<byte>();
            foreach(char item in c)
            {
                byte[] b = ConvertCharToByte(item);
                result.AddRange(b);
            }

            return result.ToArray();
        }

        public byte[] ConvertCharToByte(char c)
        {
            byte[] b = new byte[2];
            b[0] = (byte)((c & 0xFF00) >> 8);
            b[1] = (byte)(c & 0xFF);
            return b;
        }

        public byte[] ConvertUintToByte(uint val, int len = 4)
        {
            byte[] result = new byte[len];
            for(int i=0; i< len;i++)
            {
                System.Int32 offset = (System.Int32)(8 * i);
                int j = len - i - 1;
                result[j] = (byte)((val >> offset) & 0xFF);
            }
            return result;
        }

        public byte[] ConvertUshortToByte(ushort val, int len = 2)
        {
            byte[] result = new byte[len];
            for (int i = 0; i < len; i++)
            {
                System.Int16 offset = (System.Int16)(8 * i);
                int j = len - i - 1;
                result[j] = (byte)((val >> offset) & 0xFF);
            }
            return result;
        }

        public byte[] Reverse(byte[] data)
        {
            if(data == null || data.Length == 0)
            {
                return data;
            }
            int len = data.Length;
            byte[] result = new byte[len];
            int j = len;
            for(int i=0;i<len;i++)
            {
                j--;
                result[i] = data[j];
            }
            return result;
        }

        public bool CheckPrefix(byte[] data, byte[] prefix, out byte[] check)
        {
            check = data;
            if (data == null || data.Length == 0)
            {
                return false;
            }
            if (prefix == null || prefix.Length == 0)
            {
                return true;
            }
            if (data.Length < prefix.Length)
            {
                return false;
            }

            var newValue = new List<byte>();
            //int result = 0;
            int j = 0;
            for(int i=0;i<data.Length;i++)
            {
                if(j<prefix.Length && data[i] == prefix[j])
                {
                    j++;
                    //result++;
                    newValue.Add(data[i]);
                    continue;
                }
                if(j>= prefix.Length)
                {
                    newValue.Add(data[i]);
                }
            }

            if(j < prefix.Length)
            {
                return false;
            }

            check = newValue.ToArray();
            return true;
        }

        //public int ConvertByteToInt(byte[] data, int index = 0)
        //{
        //    int result = 0;
        //    try
        //    {
        //        result = BitConverter.ToInt32(data, index);
        //    }
        //    catch(Exception ex) 
        //    {
            
        //    }

        //    return result;
        //}

        public double ConvertByteToDouble(byte[] data, int digit= 1)
        {
            if (data == null || data.Length == 0)
            {
                return 0;
            }

            //int val = ConvertByteToInt(data, 0);
            int val = 0;
            bool is_negative = Formulars.Instance.GetHighDigit(data) == 1;
            for (int i = 0; i < data.Length; i++)
            {
                int offset = 8 * i;
                val = val << offset;
                byte item = is_negative ? (byte)~data[i] : (byte)data[i];
                val |= item;
            }

            if(is_negative)
            {
                val += 1;
                val *= -1;
            }

            double result = (double)val * Math.Pow(10, digit * -1);
            return result;
            //byte[] result = new byte[data.Length];
            //data.CopyTo(result, 0);

            //bool is_negative = Formulars.Instance.GetHighDigit(data) > 0;

            //if (is_negative)
            //{
            //    result = Formulars.Instance.InvertCode(result);
            //    result = Formulars.Instance.ComplementCode(result);
            //}

            //double val = Formulars.Instance.ConvertByteToInt(result);
            //val *= is_negative ? -1 : 1;
            //val = val * Math.Pow(10, digit * -1);

            //return val;
        }

        public byte[] ConvertDoubleToByte(double data, int digit = 1, int len=2)
        {
            bool is_negative = data < 0;
            if (is_negative)
            {
                data = data * -1;
                //data -= 1;
            }

            int val = (int)(data * Math.Pow(10, digit));

            if(is_negative)
            {
                val -= 1;
            }

            byte[] result = Formulars.Instance.ConvertIntToX16(val);

            result = Formulars.Instance.PaddingByte(result, len);

            if (is_negative)
            {
                result = Formulars.Instance.InvertCode(result);
                //result = Formulars.Instance.ComplementCode(result, 1);
            }

            return result;
        }
    }
}
