﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class DataTableExtension
    {
        public static void DoSomethingForEach(this DataTable data, Action<DataRow> action)
        {
            if (data == null || data.Rows == null || data.Rows.Count == 0 || action == null)
            {
                return;
            }
            foreach (DataRow item in data.Rows)
            {
                if (item == null) continue;
                action?.Invoke(item);
            }
        }

        public static string ToJsonString(this DataTable data)
        {
            if (data == null || data.Rows == null || data.Rows.Count == 0)
            {
                return string.Empty;
            }
            StringBuilder sb = new StringBuilder();
            List<string> cols = new List<string>();
            foreach(DataColumn item in data.Columns)
            {
                cols.Add(item.ColumnName);
            }
            sb.Append($"\"{data.TableName}\":[");
            data.DoSomethingForEach((item) =>
            {
                sb.Append("{");
                object[] array = item.ItemArray;
                int len = array.Length;
                for (int i=0;i<len;i++)
                {
                    string val = array[i].ToString();
                    sb.Append($"\"{cols[i]}\":\"{val}\"");
                    if (i == len - 1) continue;
                    sb.Append(",");
                }
                sb.Append("},\r\n");
            });
            sb.Append("]\r\n");

            return sb.ToString();
        }
    }
}
