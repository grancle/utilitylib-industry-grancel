﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class ConcurrentBagHelper
    {
        public static void Remove<T>(this ConcurrentBag<T> bag, T item)
        {
            while (!bag.IsEmpty)
            {
                T result;
                bag.TryTake(out result);

                if (result.Equals(item))
                {
                    break;
                }

                bag.Add(result);
            }
        }
    }
}
