﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UtilityLib
{
    public class RegexHelper
    {
        public RegexHelper(string RegexStr)
        {
            this.RegexStr = RegexStr;
        }

        public RegexHelper() { }

        public string RegexStr { get; set; }

        public bool IsMatch(string text)
        {
            try
            {
                return IsMatchImpl(text);
            }
            catch
            {
                return false;
            }

        }

        private bool IsMatchImpl(string text)
        {
            if (string.IsNullOrEmpty(RegexStr))
            {
                return true;
            }

            Match result = CalMatch(text);
            if(result == null)
            {
                return false;
            }
            return result.Success;
        }

        private Match CalMatch(string text)
        {
            if(string.IsNullOrEmpty(text))
            {
                return null;
            }
            Match result = Regex.Match(text, RegexStr);
            return result;
        }
    }
}
