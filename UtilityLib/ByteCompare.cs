﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class ByteCompare
    {
        public static bool AreEqualWithPrefix(this byte[] data, byte[] prefix)
        {
            if(data == null || data.Length==0)
            {
                return false;
            }
            if(prefix == null || prefix.Length ==0)
            {
                return true;
            }
            if(data.Length < prefix.Length)
            {
                return false;
            }
            for(int i = 0;i <prefix.Length;i++)
            {
                try
                {
                    bool chk = prefix[i] == data[i];
                    if(!chk)
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }
    }
}
