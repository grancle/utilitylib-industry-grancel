﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class MathHelper
    {
        public static MathHelper Instance { get; } = new MathHelper();

        public decimal CalPPM(int central, decimal ppm)
        {
            try
            {
                return CalPPMImpl(central, ppm);
            }
            catch 
            {
                return 0;
            }
        }

        private decimal CalPPMImpl(int central, decimal ppm)
        {
            decimal result = (decimal)Math.Pow(10, -6);
            result = result * ppm;
            result = (decimal)central * result;
            return result;
        }
    }
}
