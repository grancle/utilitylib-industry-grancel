﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class MacroSimpleHelper
    {
        public MacroSimpleHelper() 
        {
            AddMacroLenSymbol();
        }
        public MacroSimpleHelper(string SrcValue)
        {
            this.SrcValue = SrcValue;
        }

        public string SrcValue { get; private set; }

        public void AddMacroLenSymbol()
        {
            this.SrcValue += FIELD_LEN;
            //Convert();
        }

        public void Convert()
        {
            if(string.IsNullOrEmpty(SrcValue))
            {
                return;
            }

            if(SrcValue == "0")
            {
                CalValue = 0;
                IsValid = true;
                return;
            }

            int val = Formulars.Instance.ConvertStrToInt(SrcValue);
            if(val != 0)
            {
                CalValue = val;
                IsValid = true;
                return;
            }

            string yu = SrcValue;
            if(SrcValue.Contains("+"))
            {
                is_add = true;
                yu = yu.Replace("+", "");
            }
            else if(SrcValue.Contains("-"))
            {
                is_add = false;
                yu = yu.Replace("-", "");
            }

            if(SrcValue.StartsWith(this.FIELD_LEN))
            {
                is_len_at_left = true;
                yu = yu.Replace(this.FIELD_LEN, "");
            }
            else if(SrcValue.EndsWith(this.FIELD_LEN))
            {
                is_len_at_left = false;
                yu = yu.Replace(this.FIELD_LEN, "");
            }

            if(string.IsNullOrEmpty(yu) || yu == "0")
            {
                CalValue = 0;
                IsValid = true;
                return;
            }

            val = Formulars.Instance.ConvertStrToInt(yu);
            if (val == 0)
            {
                return;
            }

            CalValue = val;
            IsValid = true;
        }

        public bool IsValid { get; private set; } = false;

        public bool IsNumberOnly
        {
            get
            {
                return is_add == null;
            }
        }

        public int Len { get; private set; } = 0;

        public bool? is_add { get; private set; } = null;

        public string FIELD_LEN = "{len}";

        public bool? is_len_at_left { get; private set; } = null;

        public int CalValue { get; private set; }

        public void Calculator(int Len = 0)
        {
            if(!IsValid)
            {
                return;
            }

            if(IsNumberOnly)
            {
                ResultValue = CalValue;
                return;
            }

            if(is_add == true)
            {
                ResultValue = CalValue + Len;
                return;
            }

            if(is_add == false)
            {
                if(is_len_at_left == true)
                {
                    ResultValue = Len - CalValue;
                    return;
                }
                else if(is_len_at_left == false)
                {
                    ResultValue = CalValue - Len;
                    return;
                }
            }
        }

        public int ResultValue { get; private set; }
    }
}
