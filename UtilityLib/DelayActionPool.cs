﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class DelayActionPool
    {
        private DelayActionPool() 
        {
            MyDelayArray = new ConcurrentDictionary<string, DelayActionHelper>();
        }

        private static DelayActionPool _instance = null;
        public static DelayActionPool Instance
        {
            get
            {
                if(_instance == null)
                {
                    lock(_locker)
                    {
                        if(_instance == null)
                        {
                            _instance = new DelayActionPool();
                        }
                    }
                }
                return _instance;
            }
        }

        private static readonly object _locker = new object();

        public ConcurrentDictionary<string, DelayActionHelper> MyDelayArray { get; private set; }

        public void Debounce(string name, int timeMs, ISynchronizeInvoke invoker, Action action)
        {
            DelayActionHelper helper = GetDelayHelper(name);
            if (helper == null) return;
            helper.Debounce(timeMs, invoker, action);
        }

        public void Throttle(string name, int timeMs, ISynchronizeInvoke invoker, Action action)
        {
            DelayActionHelper helper = GetDelayHelper(name);
            if (helper == null) return;
            helper.Throttle(timeMs, invoker, action);
        }

        private DelayActionHelper GetDelayHelper(string name)
        {
            DelayActionHelper result = null;
            lock (_locker)
            {
                if(MyDelayArray.ContainsKey(name))
                {
                    MyDelayArray.TryGetValue(name, out result);
                }
                else
                {
                    result = new DelayActionHelper();
                    MyDelayArray.TryAdd(name, result);
                }
            }
            return result;
        }
    }
}
