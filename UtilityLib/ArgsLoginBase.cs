﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public abstract class ArgsLoginBase
    {
        protected ArgsLoginBase()
        {
            DealWithArgs();
        }

        protected virtual void DealWithArgs()
        {
            try
            {
                DealWithArgsImpl();
            }
            catch { }
        }

        protected virtual void DealWithArgsImpl()
        {
            string[] args = Environment.GetCommandLineArgs();

            foreach(var item in args)
            {
                DealWithArgsImpl(item);
            }

        }

        protected virtual void DealWithArgsImpl(string item)
        {
            GetUserName(item);
        }

        public string UserName { get; protected set; }
        protected virtual void GetUserName(string argv)
        {
            GetValue(argv, "-u=", (val) => {
                this.UserName = val;
            });
        }

        protected virtual string GetValue(string argv, string starts)
        {
            if (string.IsNullOrEmpty(argv) || string.IsNullOrEmpty(starts) 
                        || !argv.StartsWith(starts))
            {
                return string.Empty;
            }

            string result = string.Empty;
            try
            {
                result = argv.Substring(starts.Length, argv.Length - starts.Length);
            }
            catch { }
            return result;
        }

        protected virtual void GetValue(string argv, string starts, Action<string> action)
        {
            string val = GetValue(argv, starts);
            if (string.IsNullOrEmpty(val))
            {
                return;
            }
            action.Invoke(val);
        }
    }
}
