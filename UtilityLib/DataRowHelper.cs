﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class DataRowHelper
    {
        public static object GetValue(this DataRow row, string columnName)
        {
            if (row == null || string.IsNullOrEmpty(columnName))
            {
                return null;
            }
            object result = null;
            try
            {
                result = row[columnName];
            }
            catch { }
            return result;
        }

        public static T GetValue<T>(this DataRow row, string columnName)
        {
            object result = row.GetValue(columnName);
            return result.ConvertHard<T>();
        }
    }
}
