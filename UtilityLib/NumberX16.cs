﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class NumberX16
    {
        public NumberX16(int MyNumberX10)
        {
            this.MyNumberX10 = MyNumberX10;
            ConvertToX16();
        }

        public NumberX16(byte[] MyData)
        {
            this.MyData = MyData;
            ConvertToX10();
        }

        public NumberX16(byte data)
        {
            this.MyData = new byte[] { data };
            ConvertToX10();
        }

        public NumberX16(string data)
        {
            ConvertStrToX16(data);
            ConvertToX10();
        }

        public byte[] MyData { get; private set; }

        public int MyNumberX10 { get; private set; }

        public byte[] MyDataToTwoByte
        {
            get
            {
                byte[] result = new byte[2] { 0x00, 0x00 };
                if(MyData != null)
                {
                    if(MyData.Length >=1)
                    {
                        result[1] = MyData[MyData.Length - 1];
                    }
                    if(MyData.Length >=2)
                    {
                        result[0] = MyData[MyData.Length - 2];
                    }
                }
                return result;
            }
        }

        public byte MyDataLastByte
        {
            get
            {
                byte result = 0x00;
                if(MyData != null)
                {
                    result = MyData[MyData.Length - 1];
                }
                return result;
            }
        }

        private void ConvertToX16()
        {
            this.MyData = ConvertIntToX16(this.MyNumberX10);
        }

        private byte[] ConvertIntToX16(int src)
        {
            List<byte> result = new List<byte>();
            try
            {
                while (true)
                {
                    int remainder = src % 256;
                    byte buff = ConvertIntToX16Single(remainder);
                    result.Add(buff);
                    int quotient = src / 256;
                    if (quotient == 0) break;
                    src = quotient;
                }
                result.Reverse();
            }
            catch { }

            return result.ToArray();
        }


        private void ConvertToX10()
        {
            this.MyNumberX10 = ConvertX16ToInt(this.MyData);
        }

        private int ConvertX16ToInt(byte[] src)
        {
            if(src == null || src.Length ==0)
            {
                return 0;
            }
            int result = 0;
            try
            {
                int index = 0;
                for(int i=src.Length-1;i>=0;i--)
                {
                    byte x16 = src[i];
                    int x10 = ConvertX16ToIntSingle(x16);
                    result += x10 * (int)Math.Pow(256, index);
                    index++;
                }
            }
            catch { }
            return result;
        }

        public override string ToString()
        {
            return Formulars.Instance.ConvertByteToStr(this.MyData, Encoding.UTF8);
        }

        public static bool operator ==(NumberX16 a, NumberX16 b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(NumberX16 a, NumberX16 b)
        {
            return !a.Equals(b);
        }

        public bool Equals(NumberX16 other)
        {
            if (ReferenceEquals(this, null) && ReferenceEquals(other, null)) return true;
            if (ReferenceEquals(this, null) || ReferenceEquals(other, null)) return false;

            if (this.MyData == null && other.MyData == null) return true;
            if (this.MyData == null || other.MyData == null) return false;
            if (this.MyData.Length != other.MyData.Length) return false;

            for (int i =0;i<this.MyData.Length;i++)
            {
                if (other.MyData.Length <= i) return false;
                if (this.MyData[i] != other.MyData[i]) return false;
            }

            return this.MyNumberX10 == other.MyNumberX10;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, null) && ReferenceEquals(obj, null)) return true;
            if (ReferenceEquals(this, null) || ReferenceEquals(obj, null)) return false;

            return Equals(obj as NumberX16);
        }

        public override int GetHashCode()
        {
            return StringComparer.InvariantCulture.GetHashCode(this.MyNumberX10);
        }

        public static NumberX16 operator +(NumberX16 a, NumberX16 b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null)) return null;
            if (ReferenceEquals(a, null)) return b;
            if (ReferenceEquals(b, null)) return a;

            int val = a.MyNumberX10 + b.MyNumberX10;
            NumberX16 result = new NumberX16(val);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>return null if result < 0;</returns>
        public static NumberX16 operator -(NumberX16 a, NumberX16 b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null)) return null;
            if (ReferenceEquals(a, null)) return null;
            if (ReferenceEquals(b, null)) return a;

            int val = a.MyNumberX10 - b.MyNumberX10;
            if (val < 0) return null;
            NumberX16 result = new NumberX16(val);
            return result;
        }

        #region func_single
        private byte ConvertIntToX16Single(int src)
        {
            byte result = 0x00;
            try
            {
                switch (src)
                {
                    case 0:
                        result = 0x00;
                        break;
                    case 1:
                        result = 0x01;
                        break;
                    case 2:
                        result = 0x02;
                        break;
                    case 3:
                        result = 0x03;
                        break;
                    case 4:
                        result = 0x04;
                        break;
                    case 5:
                        result = 0x05;
                        break;
                    case 6:
                        result = 0x06;
                        break;
                    case 7:
                        result = 0x07;
                        break;
                    case 8:
                        result = 0x08;
                        break;
                    case 9:
                        result = 0x09;
                        break;
                    case 10:
                        result = 0x0A;
                        break;
                    case 11:
                        result = 0x0B;
                        break;
                    case 12:
                        result = 0x0C;
                        break;
                    case 13:
                        result = 0x0D;
                        break;
                    case 14:
                        result = 0x0E;
                        break;
                    case 15:
                        result = 0x0F;
                        break;
                    case 16:
                        result = 0x10;
                        break;
                    case 17:
                        result = 0x11;
                        break;
                    case 18:
                        result = 0x12;
                        break;
                    case 19:
                        result = 0x13;
                        break;
                    case 20:
                        result = 0x14;
                        break;
                    case 21:
                        result = 0x15;
                        break;
                    case 22:
                        result = 0x16;
                        break;
                    case 23:
                        result = 0x17;
                        break;
                    case 24:
                        result = 0x18;
                        break;
                    case 25:
                        result = 0x19;
                        break;
                    case 26:
                        result = 0x1A;
                        break;
                    case 27:
                        result = 0x1B;
                        break;
                    case 28:
                        result = 0x1C;
                        break;
                    case 29:
                        result = 0x1D;
                        break;
                    case 30:
                        result = 0x1E;
                        break;
                    case 31:
                        result = 0x1F;
                        break;
                    case 32:
                        result = 0x20;
                        break;
                    case 33:
                        result = 0x21;
                        break;
                    case 34:
                        result = 0x22;
                        break;
                    case 35:
                        result = 0x23;
                        break;
                    case 36:
                        result = 0x24;
                        break;
                    case 37:
                        result = 0x25;
                        break;
                    case 38:
                        result = 0x26;
                        break;
                    case 39:
                        result = 0x27;
                        break;
                    case 40:
                        result = 0x28;
                        break;
                    case 41:
                        result = 0x29;
                        break;
                    case 42:
                        result = 0x2A;
                        break;
                    case 43:
                        result = 0x2B;
                        break;
                    case 44:
                        result = 0x2C;
                        break;
                    case 45:
                        result = 0x2D;
                        break;
                    case 46:
                        result = 0x2E;
                        break;
                    case 47:
                        result = 0x2F;
                        break;
                    case 48:
                        result = 0x30;
                        break;
                    case 49:
                        result = 0x31;
                        break;
                    case 50:
                        result = 0x32;
                        break;
                    case 51:
                        result = 0x33;
                        break;
                    case 52:
                        result = 0x34;
                        break;
                    case 53:
                        result = 0x35;
                        break;
                    case 54:
                        result = 0x36;
                        break;
                    case 55:
                        result = 0x37;
                        break;
                    case 56:
                        result = 0x38;
                        break;
                    case 57:
                        result = 0x39;
                        break;
                    case 58:
                        result = 0x3A;
                        break;
                    case 59:
                        result = 0x3B;
                        break;
                    case 60:
                        result = 0x3C;
                        break;
                    case 61:
                        result = 0x3D;
                        break;
                    case 62:
                        result = 0x3E;
                        break;
                    case 63:
                        result = 0x3F;
                        break;
                    case 64:
                        result = 0x40;
                        break;
                    case 65:
                        result = 0x41;
                        break;
                    case 66:
                        result = 0x42;
                        break;
                    case 67:
                        result = 0x43;
                        break;
                    case 68:
                        result = 0x44;
                        break;
                    case 69:
                        result = 0x45;
                        break;
                    case 70:
                        result = 0x46;
                        break;
                    case 71:
                        result = 0x47;
                        break;
                    case 72:
                        result = 0x48;
                        break;
                    case 73:
                        result = 0x49;
                        break;
                    case 74:
                        result = 0x4A;
                        break;
                    case 75:
                        result = 0x4B;
                        break;
                    case 76:
                        result = 0x4C;
                        break;
                    case 77:
                        result = 0x4D;
                        break;
                    case 78:
                        result = 0x4E;
                        break;
                    case 79:
                        result = 0x4F;
                        break;
                    case 80:
                        result = 0x50;
                        break;
                    case 81:
                        result = 0x51;
                        break;
                    case 82:
                        result = 0x52;
                        break;
                    case 83:
                        result = 0x53;
                        break;
                    case 84:
                        result = 0x54;
                        break;
                    case 85:
                        result = 0x55;
                        break;
                    case 86:
                        result = 0x56;
                        break;
                    case 87:
                        result = 0x57;
                        break;
                    case 88:
                        result = 0x58;
                        break;
                    case 89:
                        result = 0x59;
                        break;
                    case 90:
                        result = 0x5A;
                        break;
                    case 91:
                        result = 0x5B;
                        break;
                    case 92:
                        result = 0x5C;
                        break;
                    case 93:
                        result = 0x5D;
                        break;
                    case 94:
                        result = 0x5E;
                        break;
                    case 95:
                        result = 0x5F;
                        break;
                    case 96:
                        result = 0x60;
                        break;
                    case 97:
                        result = 0x61;
                        break;
                    case 98:
                        result = 0x62;
                        break;
                    case 99:
                        result = 0x63;
                        break;
                    case 100:
                        result = 0x64;
                        break;
                    case 101:
                        result = 0x65;
                        break;
                    case 102:
                        result = 0x66;
                        break;
                    case 103:
                        result = 0x67;
                        break;
                    case 104:
                        result = 0x68;
                        break;
                    case 105:
                        result = 0x69;
                        break;
                    case 106:
                        result = 0x6A;
                        break;
                    case 107:
                        result = 0x6B;
                        break;
                    case 108:
                        result = 0x6C;
                        break;
                    case 109:
                        result = 0x6D;
                        break;
                    case 110:
                        result = 0x6E;
                        break;
                    case 111:
                        result = 0x6F;
                        break;
                    case 112:
                        result = 0x70;
                        break;
                    case 113:
                        result = 0x71;
                        break;
                    case 114:
                        result = 0x72;
                        break;
                    case 115:
                        result = 0x73;
                        break;
                    case 116:
                        result = 0x74;
                        break;
                    case 117:
                        result = 0x75;
                        break;
                    case 118:
                        result = 0x76;
                        break;
                    case 119:
                        result = 0x77;
                        break;
                    case 120:
                        result = 0x78;
                        break;
                    case 121:
                        result = 0x79;
                        break;
                    case 122:
                        result = 0x7A;
                        break;
                    case 123:
                        result = 0x7B;
                        break;
                    case 124:
                        result = 0x7C;
                        break;
                    case 125:
                        result = 0x7D;
                        break;
                    case 126:
                        result = 0x7E;
                        break;
                    case 127:
                        result = 0x7F;
                        break;
                    case 128:
                        result = 0x80;
                        break;
                    case 129:
                        result = 0x81;
                        break;
                    case 130:
                        result = 0x82;
                        break;
                    case 131:
                        result = 0x83;
                        break;
                    case 132:
                        result = 0x84;
                        break;
                    case 133:
                        result = 0x85;
                        break;
                    case 134:
                        result = 0x86;
                        break;
                    case 135:
                        result = 0x87;
                        break;
                    case 136:
                        result = 0x88;
                        break;
                    case 137:
                        result = 0x89;
                        break;
                    case 138:
                        result = 0x8A;
                        break;
                    case 139:
                        result = 0x8B;
                        break;
                    case 140:
                        result = 0x8C;
                        break;
                    case 141:
                        result = 0x8D;
                        break;
                    case 142:
                        result = 0x8E;
                        break;
                    case 143:
                        result = 0x8F;
                        break;
                    case 144:
                        result = 0x90;
                        break;
                    case 145:
                        result = 0x91;
                        break;
                    case 146:
                        result = 0x92;
                        break;
                    case 147:
                        result = 0x93;
                        break;
                    case 148:
                        result = 0x94;
                        break;
                    case 149:
                        result = 0x95;
                        break;
                    case 150:
                        result = 0x96;
                        break;
                    case 151:
                        result = 0x97;
                        break;
                    case 152:
                        result = 0x98;
                        break;
                    case 153:
                        result = 0x99;
                        break;
                    case 154:
                        result = 0x9A;
                        break;
                    case 155:
                        result = 0x9B;
                        break;
                    case 156:
                        result = 0x9C;
                        break;
                    case 157:
                        result = 0x9D;
                        break;
                    case 158:
                        result = 0x9E;
                        break;
                    case 159:
                        result = 0x9F;
                        break;
                    case 160:
                        result = 0xA0;
                        break;
                    case 161:
                        result = 0xA1;
                        break;
                    case 162:
                        result = 0xA2;
                        break;
                    case 163:
                        result = 0xA3;
                        break;
                    case 164:
                        result = 0xA4;
                        break;
                    case 165:
                        result = 0xA5;
                        break;
                    case 166:
                        result = 0xA6;
                        break;
                    case 167:
                        result = 0xA7;
                        break;
                    case 168:
                        result = 0xA8;
                        break;
                    case 169:
                        result = 0xA9;
                        break;
                    case 170:
                        result = 0xAA;
                        break;
                    case 171:
                        result = 0xAB;
                        break;
                    case 172:
                        result = 0xAC;
                        break;
                    case 173:
                        result = 0xAD;
                        break;
                    case 174:
                        result = 0xAE;
                        break;
                    case 175:
                        result = 0xAF;
                        break;
                    case 176:
                        result = 0xB0;
                        break;
                    case 177:
                        result = 0xB1;
                        break;
                    case 178:
                        result = 0xB2;
                        break;
                    case 179:
                        result = 0xB3;
                        break;
                    case 180:
                        result = 0xB4;
                        break;
                    case 181:
                        result = 0xB5;
                        break;
                    case 182:
                        result = 0xB6;
                        break;
                    case 183:
                        result = 0xB7;
                        break;
                    case 184:
                        result = 0xB8;
                        break;
                    case 185:
                        result = 0xB9;
                        break;
                    case 186:
                        result = 0xBA;
                        break;
                    case 187:
                        result = 0xBB;
                        break;
                    case 188:
                        result = 0xBC;
                        break;
                    case 189:
                        result = 0xBD;
                        break;
                    case 190:
                        result = 0xBE;
                        break;
                    case 191:
                        result = 0xBF;
                        break;
                    case 192:
                        result = 0xC0;
                        break;
                    case 193:
                        result = 0xC1;
                        break;
                    case 194:
                        result = 0xC2;
                        break;
                    case 195:
                        result = 0xC3;
                        break;
                    case 196:
                        result = 0xC4;
                        break;
                    case 197:
                        result = 0xC5;
                        break;
                    case 198:
                        result = 0xC6;
                        break;
                    case 199:
                        result = 0xC7;
                        break;
                    case 200:
                        result = 0xC8;
                        break;
                    case 201:
                        result = 0xC9;
                        break;
                    case 202:
                        result = 0xCA;
                        break;
                    case 203:
                        result = 0xCB;
                        break;
                    case 204:
                        result = 0xCC;
                        break;
                    case 205:
                        result = 0xCD;
                        break;
                    case 206:
                        result = 0xCE;
                        break;
                    case 207:
                        result = 0xCF;
                        break;
                    case 208:
                        result = 0xD0;
                        break;
                    case 209:
                        result = 0xD1;
                        break;
                    case 210:
                        result = 0xD2;
                        break;
                    case 211:
                        result = 0xD3;
                        break;
                    case 212:
                        result = 0xD4;
                        break;
                    case 213:
                        result = 0xD5;
                        break;
                    case 214:
                        result = 0xD6;
                        break;
                    case 215:
                        result = 0xD7;
                        break;
                    case 216:
                        result = 0xD8;
                        break;
                    case 217:
                        result = 0xD9;
                        break;
                    case 218:
                        result = 0xDA;
                        break;
                    case 219:
                        result = 0xDB;
                        break;
                    case 220:
                        result = 0xDC;
                        break;
                    case 221:
                        result = 0xDD;
                        break;
                    case 222:
                        result = 0xDE;
                        break;
                    case 223:
                        result = 0xDF;
                        break;
                    case 224:
                        result = 0xE0;
                        break;
                    case 225:
                        result = 0xE1;
                        break;
                    case 226:
                        result = 0xE2;
                        break;
                    case 227:
                        result = 0xE3;
                        break;
                    case 228:
                        result = 0xE4;
                        break;
                    case 229:
                        result = 0xE5;
                        break;
                    case 230:
                        result = 0xE6;
                        break;
                    case 231:
                        result = 0xE7;
                        break;
                    case 232:
                        result = 0xE8;
                        break;
                    case 233:
                        result = 0xE9;
                        break;
                    case 234:
                        result = 0xEA;
                        break;
                    case 235:
                        result = 0xEB;
                        break;
                    case 236:
                        result = 0xEC;
                        break;
                    case 237:
                        result = 0xED;
                        break;
                    case 238:
                        result = 0xEE;
                        break;
                    case 239:
                        result = 0xEF;
                        break;
                    case 240:
                        result = 0xF0;
                        break;
                    case 241:
                        result = 0xF1;
                        break;
                    case 242:
                        result = 0xF2;
                        break;
                    case 243:
                        result = 0xF3;
                        break;
                    case 244:
                        result = 0xF4;
                        break;
                    case 245:
                        result = 0xF5;
                        break;
                    case 246:
                        result = 0xF6;
                        break;
                    case 247:
                        result = 0xF7;
                        break;
                    case 248:
                        result = 0xF8;
                        break;
                    case 249:
                        result = 0xF9;
                        break;
                    case 250:
                        result = 0xFA;
                        break;
                    case 251:
                        result = 0xFB;
                        break;
                    case 252:
                        result = 0xFC;
                        break;
                    case 253:
                        result = 0xFD;
                        break;
                    case 254:
                        result = 0xFE;
                        break;
                    case 255:
                        result = 0xFF;
                        break;
                    //case 0:
                    default:
                        break;
                }
            }
            catch { }
            return result;
        }
        private int ConvertX16ToIntSingle(byte src)
        {
            int result = 0;
            switch(src)
            {
                case 0x00:
                    result = 0;
                    break;
                case 0x01:
                    result = 1;
                    break;
                case 0x02:
                    result = 2;
                    break;
                case 0x03:
                    result = 3;
                    break;
                case 0x04:
                    result = 4;
                    break;
                case 0x05:
                    result = 5;
                    break;
                case 0x06:
                    result = 6;
                    break;
                case 0x07:
                    result = 7;
                    break;
                case 0x08:
                    result = 8;
                    break;
                case 0x09:
                    result = 9;
                    break;
                case 0x0A:
                    result = 10;
                    break;
                case 0x0B:
                    result = 11;
                    break;
                case 0x0C:
                    result = 12;
                    break;
                case 0x0D:
                    result = 13;
                    break;
                case 0x0E:
                    result = 14;
                    break;
                case 0x0F:
                    result = 15;
                    break;
                case 0x10:
                    result = 16;
                    break;
                case 0x11:
                    result = 17;
                    break;
                case 0x12:
                    result = 18;
                    break;
                case 0x13:
                    result = 19;
                    break;
                case 0x14:
                    result = 20;
                    break;
                case 0x15:
                    result = 21;
                    break;
                case 0x16:
                    result = 22;
                    break;
                case 0x17:
                    result = 23;
                    break;
                case 0x18:
                    result = 24;
                    break;
                case 0x19:
                    result = 25;
                    break;
                case 0x1A:
                    result = 26;
                    break;
                case 0x1B:
                    result = 27;
                    break;
                case 0x1C:
                    result = 28;
                    break;
                case 0x1D:
                    result = 29;
                    break;
                case 0x1E:
                    result = 30;
                    break;
                case 0x1F:
                    result = 31;
                    break;
                case 0x20:
                    result = 32;
                    break;
                case 0x21:
                    result = 33;
                    break;
                case 0x22:
                    result = 34;
                    break;
                case 0x23:
                    result = 35;
                    break;
                case 0x24:
                    result = 36;
                    break;
                case 0x25:
                    result = 37;
                    break;
                case 0x26:
                    result = 38;
                    break;
                case 0x27:
                    result = 39;
                    break;
                case 0x28:
                    result = 40;
                    break;
                case 0x29:
                    result = 41;
                    break;
                case 0x2A:
                    result = 42;
                    break;
                case 0x2B:
                    result = 43;
                    break;
                case 0x2C:
                    result = 44;
                    break;
                case 0x2D:
                    result = 45;
                    break;
                case 0x2E:
                    result = 46;
                    break;
                case 0x2F:
                    result = 47;
                    break;
                case 0x30:
                    result = 48;
                    break;
                case 0x31:
                    result = 49;
                    break;
                case 0x32:
                    result = 50;
                    break;
                case 0x33:
                    result = 51;
                    break;
                case 0x34:
                    result = 52;
                    break;
                case 0x35:
                    result = 53;
                    break;
                case 0x36:
                    result = 54;
                    break;
                case 0x37:
                    result = 55;
                    break;
                case 0x38:
                    result = 56;
                    break;
                case 0x39:
                    result = 57;
                    break;
                case 0x3A:
                    result = 58;
                    break;
                case 0x3B:
                    result = 59;
                    break;
                case 0x3C:
                    result = 60;
                    break;
                case 0x3D:
                    result = 61;
                    break;
                case 0x3E:
                    result = 62;
                    break;
                case 0x3F:
                    result = 63;
                    break;
                case 0x40:
                    result = 64;
                    break;
                case 0x41:
                    result = 65;
                    break;
                case 0x42:
                    result = 66;
                    break;
                case 0x43:
                    result = 67;
                    break;
                case 0x44:
                    result = 68;
                    break;
                case 0x45:
                    result = 69;
                    break;
                case 0x46:
                    result = 70;
                    break;
                case 0x47:
                    result = 71;
                    break;
                case 0x48:
                    result = 72;
                    break;
                case 0x49:
                    result = 73;
                    break;
                case 0x4A:
                    result = 74;
                    break;
                case 0x4B:
                    result = 75;
                    break;
                case 0x4C:
                    result = 76;
                    break;
                case 0x4D:
                    result = 77;
                    break;
                case 0x4E:
                    result = 78;
                    break;
                case 0x4F:
                    result = 79;
                    break;
                case 0x50:
                    result = 80;
                    break;
                case 0x51:
                    result = 81;
                    break;
                case 0x52:
                    result = 82;
                    break;
                case 0x53:
                    result = 83;
                    break;
                case 0x54:
                    result = 84;
                    break;
                case 0x55:
                    result = 85;
                    break;
                case 0x56:
                    result = 86;
                    break;
                case 0x57:
                    result = 87;
                    break;
                case 0x58:
                    result = 88;
                    break;
                case 0x59:
                    result = 89;
                    break;
                case 0x5A:
                    result = 90;
                    break;
                case 0x5B:
                    result = 91;
                    break;
                case 0x5C:
                    result = 92;
                    break;
                case 0x5D:
                    result = 93;
                    break;
                case 0x5E:
                    result = 94;
                    break;
                case 0x5F:
                    result = 95;
                    break;
                case 0x60:
                    result = 96;
                    break;
                case 0x61:
                    result = 97;
                    break;
                case 0x62:
                    result = 98;
                    break;
                case 0x63:
                    result = 99;
                    break;
                case 0x64:
                    result = 100;
                    break;
                case 0x65:
                    result = 101;
                    break;
                case 0x66:
                    result = 102;
                    break;
                case 0x67:
                    result = 103;
                    break;
                case 0x68:
                    result = 104;
                    break;
                case 0x69:
                    result = 105;
                    break;
                case 0x6A:
                    result = 106;
                    break;
                case 0x6B:
                    result = 107;
                    break;
                case 0x6C:
                    result = 108;
                    break;
                case 0x6D:
                    result = 109;
                    break;
                case 0x6E:
                    result = 110;
                    break;
                case 0x6F:
                    result = 111;
                    break;
                case 0x70:
                    result = 112;
                    break;
                case 0x71:
                    result = 113;
                    break;
                case 0x72:
                    result = 114;
                    break;
                case 0x73:
                    result = 115;
                    break;
                case 0x74:
                    result = 116;
                    break;
                case 0x75:
                    result = 117;
                    break;
                case 0x76:
                    result = 118;
                    break;
                case 0x77:
                    result = 119;
                    break;
                case 0x78:
                    result = 120;
                    break;
                case 0x79:
                    result = 121;
                    break;
                case 0x7A:
                    result = 122;
                    break;
                case 0x7B:
                    result = 123;
                    break;
                case 0x7C:
                    result = 124;
                    break;
                case 0x7D:
                    result = 125;
                    break;
                case 0x7E:
                    result = 126;
                    break;
                case 0x7F:
                    result = 127;
                    break;
                case 0x80:
                    result = 128;
                    break;
                case 0x81:
                    result = 129;
                    break;
                case 0x82:
                    result = 130;
                    break;
                case 0x83:
                    result = 131;
                    break;
                case 0x84:
                    result = 132;
                    break;
                case 0x85:
                    result = 133;
                    break;
                case 0x86:
                    result = 134;
                    break;
                case 0x87:
                    result = 135;
                    break;
                case 0x88:
                    result = 136;
                    break;
                case 0x89:
                    result = 137;
                    break;
                case 0x8A:
                    result = 138;
                    break;
                case 0x8B:
                    result = 139;
                    break;
                case 0x8C:
                    result = 140;
                    break;
                case 0x8D:
                    result = 141;
                    break;
                case 0x8E:
                    result = 142;
                    break;
                case 0x8F:
                    result = 143;
                    break;
                case 0x90:
                    result = 144;
                    break;
                case 0x91:
                    result = 145;
                    break;
                case 0x92:
                    result = 146;
                    break;
                case 0x93:
                    result = 147;
                    break;
                case 0x94:
                    result = 148;
                    break;
                case 0x95:
                    result = 149;
                    break;
                case 0x96:
                    result = 150;
                    break;
                case 0x97:
                    result = 151;
                    break;
                case 0x98:
                    result = 152;
                    break;
                case 0x99:
                    result = 153;
                    break;
                case 0x9A:
                    result = 154;
                    break;
                case 0x9B:
                    result = 155;
                    break;
                case 0x9C:
                    result = 156;
                    break;
                case 0x9D:
                    result = 157;
                    break;
                case 0x9E:
                    result = 158;
                    break;
                case 0x9F:
                    result = 159;
                    break;
                case 0xA0:
                    result = 160;
                    break;
                case 0xA1:
                    result = 161;
                    break;
                case 0xA2:
                    result = 162;
                    break;
                case 0xA3:
                    result = 163;
                    break;
                case 0xA4:
                    result = 164;
                    break;
                case 0xA5:
                    result = 165;
                    break;
                case 0xA6:
                    result = 166;
                    break;
                case 0xA7:
                    result = 167;
                    break;
                case 0xA8:
                    result = 168;
                    break;
                case 0xA9:
                    result = 169;
                    break;
                case 0xAA:
                    result = 170;
                    break;
                case 0xAB:
                    result = 171;
                    break;
                case 0xAC:
                    result = 172;
                    break;
                case 0xAD:
                    result = 173;
                    break;
                case 0xAE:
                    result = 174;
                    break;
                case 0xAF:
                    result = 175;
                    break;
                case 0xB0:
                    result = 176;
                    break;
                case 0xB1:
                    result = 177;
                    break;
                case 0xB2:
                    result = 178;
                    break;
                case 0xB3:
                    result = 179;
                    break;
                case 0xB4:
                    result = 180;
                    break;
                case 0xB5:
                    result = 181;
                    break;
                case 0xB6:
                    result = 182;
                    break;
                case 0xB7:
                    result = 183;
                    break;
                case 0xB8:
                    result = 184;
                    break;
                case 0xB9:
                    result = 185;
                    break;
                case 0xBA:
                    result = 186;
                    break;
                case 0xBB:
                    result = 187;
                    break;
                case 0xBC:
                    result = 188;
                    break;
                case 0xBD:
                    result = 189;
                    break;
                case 0xBE:
                    result = 190;
                    break;
                case 0xBF:
                    result = 191;
                    break;
                case 0xC0:
                    result = 192;
                    break;
                case 0xC1:
                    result = 193;
                    break;
                case 0xC2:
                    result = 194;
                    break;
                case 0xC3:
                    result = 195;
                    break;
                case 0xC4:
                    result = 196;
                    break;
                case 0xC5:
                    result = 197;
                    break;
                case 0xC6:
                    result = 198;
                    break;
                case 0xC7:
                    result = 199;
                    break;
                case 0xC8:
                    result = 200;
                    break;
                case 0xC9:
                    result = 201;
                    break;
                case 0xCA:
                    result = 202;
                    break;
                case 0xCB:
                    result = 203;
                    break;
                case 0xCC:
                    result = 204;
                    break;
                case 0xCD:
                    result = 205;
                    break;
                case 0xCE:
                    result = 206;
                    break;
                case 0xCF:
                    result = 207;
                    break;
                case 0xD0:
                    result = 208;
                    break;
                case 0xD1:
                    result = 209;
                    break;
                case 0xD2:
                    result = 210;
                    break;
                case 0xD3:
                    result = 211;
                    break;
                case 0xD4:
                    result = 212;
                    break;
                case 0xD5:
                    result = 213;
                    break;
                case 0xD6:
                    result = 214;
                    break;
                case 0xD7:
                    result = 215;
                    break;
                case 0xD8:
                    result = 216;
                    break;
                case 0xD9:
                    result = 217;
                    break;
                case 0xDA:
                    result = 218;
                    break;
                case 0xDB:
                    result = 219;
                    break;
                case 0xDC:
                    result = 220;
                    break;
                case 0xDD:
                    result = 221;
                    break;
                case 0xDE:
                    result = 222;
                    break;
                case 0xDF:
                    result = 223;
                    break;
                case 0xE0:
                    result = 224;
                    break;
                case 0xE1:
                    result = 225;
                    break;
                case 0xE2:
                    result = 226;
                    break;
                case 0xE3:
                    result = 227;
                    break;
                case 0xE4:
                    result = 228;
                    break;
                case 0xE5:
                    result = 229;
                    break;
                case 0xE6:
                    result = 230;
                    break;
                case 0xE7:
                    result = 231;
                    break;
                case 0xE8:
                    result = 232;
                    break;
                case 0xE9:
                    result = 233;
                    break;
                case 0xEA:
                    result = 234;
                    break;
                case 0xEB:
                    result = 235;
                    break;
                case 0xEC:
                    result = 236;
                    break;
                case 0xED:
                    result = 237;
                    break;
                case 0xEE:
                    result = 238;
                    break;
                case 0xEF:
                    result = 239;
                    break;
                case 0xF0:
                    result = 240;
                    break;
                case 0xF1:
                    result = 241;
                    break;
                case 0xF2:
                    result = 242;
                    break;
                case 0xF3:
                    result = 243;
                    break;
                case 0xF4:
                    result = 244;
                    break;
                case 0xF5:
                    result = 245;
                    break;
                case 0xF6:
                    result = 246;
                    break;
                case 0xF7:
                    result = 247;
                    break;
                case 0xF8:
                    result = 248;
                    break;
                case 0xF9:
                    result = 249;
                    break;
                case 0xFA:
                    result = 250;
                    break;
                case 0xFB:
                    result = 251;
                    break;
                case 0xFC:
                    result = 252;
                    break;
                case 0xFD:
                    result = 253;
                    break;
                case 0xFE:
                    result = 254;
                    break;
                case 0xFF:
                    result = 255;
                    break;
                default:
                    break;
            }

            return result;
        }

        private byte ConvertStrToX16Single(string src)
        {
            byte result = 0x00;
            if(string.IsNullOrEmpty(src))
            {
                return result;
            }
            switch(src)
            {
                case "00":
                    result = 0x00;
                    break;
                case "01":
                    result = 0x01;
                    break;
                case "02":
                    result = 0x02;
                    break;
                case "03":
                    result = 0x03;
                    break;
                case "04":
                    result = 0x04;
                    break;
                case "05":
                    result = 0x05;
                    break;
                case "06":
                    result = 0x06;
                    break;
                case "07":
                    result = 0x07;
                    break;
                case "08":
                    result = 0x08;
                    break;
                case "09":
                    result = 0x09;
                    break;
                case "0A":
                    result = 0x0A;
                    break;
                case "0B":
                    result = 0x0B;
                    break;
                case "0C":
                    result = 0x0C;
                    break;
                case "0D":
                    result = 0x0D;
                    break;
                case "0E":
                    result = 0x0E;
                    break;
                case "0F":
                    result = 0x0F;
                    break;
                case "10":
                    result = 0x10;
                    break;
                case "11":
                    result = 0x11;
                    break;
                case "12":
                    result = 0x12;
                    break;
                case "13":
                    result = 0x13;
                    break;
                case "14":
                    result = 0x14;
                    break;
                case "15":
                    result = 0x15;
                    break;
                case "16":
                    result = 0x16;
                    break;
                case "17":
                    result = 0x17;
                    break;
                case "18":
                    result = 0x18;
                    break;
                case "19":
                    result = 0x19;
                    break;
                case "1A":
                    result = 0x1A;
                    break;
                case "1B":
                    result = 0x1B;
                    break;
                case "1C":
                    result = 0x1C;
                    break;
                case "1D":
                    result = 0x1D;
                    break;
                case "1E":
                    result = 0x1E;
                    break;
                case "1F":
                    result = 0x1F;
                    break;
                case "20":
                    result = 0x20;
                    break;
                case "21":
                    result = 0x21;
                    break;
                case "22":
                    result = 0x22;
                    break;
                case "23":
                    result = 0x23;
                    break;
                case "24":
                    result = 0x24;
                    break;
                case "25":
                    result = 0x25;
                    break;
                case "26":
                    result = 0x26;
                    break;
                case "27":
                    result = 0x27;
                    break;
                case "28":
                    result = 0x28;
                    break;
                case "29":
                    result = 0x29;
                    break;
                case "2A":
                    result = 0x2A;
                    break;
                case "2B":
                    result = 0x2B;
                    break;
                case "2C":
                    result = 0x2C;
                    break;
                case "2D":
                    result = 0x2D;
                    break;
                case "2E":
                    result = 0x2E;
                    break;
                case "2F":
                    result = 0x2F;
                    break;
                case "30":
                    result = 0x30;
                    break;
                case "31":
                    result = 0x31;
                    break;
                case "32":
                    result = 0x32;
                    break;
                case "33":
                    result = 0x33;
                    break;
                case "34":
                    result = 0x34;
                    break;
                case "35":
                    result = 0x35;
                    break;
                case "36":
                    result = 0x36;
                    break;
                case "37":
                    result = 0x37;
                    break;
                case "38":
                    result = 0x38;
                    break;
                case "39":
                    result = 0x39;
                    break;
                case "3A":
                    result = 0x3A;
                    break;
                case "3B":
                    result = 0x3B;
                    break;
                case "3C":
                    result = 0x3C;
                    break;
                case "3D":
                    result = 0x3D;
                    break;
                case "3E":
                    result = 0x3E;
                    break;
                case "3F":
                    result = 0x3F;
                    break;
                case "40":
                    result = 0x40;
                    break;
                case "41":
                    result = 0x41;
                    break;
                case "42":
                    result = 0x42;
                    break;
                case "43":
                    result = 0x43;
                    break;
                case "44":
                    result = 0x44;
                    break;
                case "45":
                    result = 0x45;
                    break;
                case "46":
                    result = 0x46;
                    break;
                case "47":
                    result = 0x47;
                    break;
                case "48":
                    result = 0x48;
                    break;
                case "49":
                    result = 0x49;
                    break;
                case "4A":
                    result = 0x4A;
                    break;
                case "4B":
                    result = 0x4B;
                    break;
                case "4C":
                    result = 0x4C;
                    break;
                case "4D":
                    result = 0x4D;
                    break;
                case "4E":
                    result = 0x4E;
                    break;
                case "4F":
                    result = 0x4F;
                    break;
                case "50":
                    result = 0x50;
                    break;
                case "51":
                    result = 0x51;
                    break;
                case "52":
                    result = 0x52;
                    break;
                case "53":
                    result = 0x53;
                    break;
                case "54":
                    result = 0x54;
                    break;
                case "55":
                    result = 0x55;
                    break;
                case "56":
                    result = 0x56;
                    break;
                case "57":
                    result = 0x57;
                    break;
                case "58":
                    result = 0x58;
                    break;
                case "59":
                    result = 0x59;
                    break;
                case "5A":
                    result = 0x5A;
                    break;
                case "5B":
                    result = 0x5B;
                    break;
                case "5C":
                    result = 0x5C;
                    break;
                case "5D":
                    result = 0x5D;
                    break;
                case "5E":
                    result = 0x5E;
                    break;
                case "5F":
                    result = 0x5F;
                    break;
                case "60":
                    result = 0x60;
                    break;
                case "61":
                    result = 0x61;
                    break;
                case "62":
                    result = 0x62;
                    break;
                case "63":
                    result = 0x63;
                    break;
                case "64":
                    result = 0x64;
                    break;
                case "65":
                    result = 0x65;
                    break;
                case "66":
                    result = 0x66;
                    break;
                case "67":
                    result = 0x67;
                    break;
                case "68":
                    result = 0x68;
                    break;
                case "69":
                    result = 0x69;
                    break;
                case "6A":
                    result = 0x6A;
                    break;
                case "6B":
                    result = 0x6B;
                    break;
                case "6C":
                    result = 0x6C;
                    break;
                case "6D":
                    result = 0x6D;
                    break;
                case "6E":
                    result = 0x6E;
                    break;
                case "6F":
                    result = 0x6F;
                    break;
                case "70":
                    result = 0x70;
                    break;
                case "71":
                    result = 0x71;
                    break;
                case "72":
                    result = 0x72;
                    break;
                case "73":
                    result = 0x73;
                    break;
                case "74":
                    result = 0x74;
                    break;
                case "75":
                    result = 0x75;
                    break;
                case "76":
                    result = 0x76;
                    break;
                case "77":
                    result = 0x77;
                    break;
                case "78":
                    result = 0x78;
                    break;
                case "79":
                    result = 0x79;
                    break;
                case "7A":
                    result = 0x7A;
                    break;
                case "7B":
                    result = 0x7B;
                    break;
                case "7C":
                    result = 0x7C;
                    break;
                case "7D":
                    result = 0x7D;
                    break;
                case "7E":
                    result = 0x7E;
                    break;
                case "7F":
                    result = 0x7F;
                    break;
                case "80":
                    result = 0x80;
                    break;
                case "81":
                    result = 0x81;
                    break;
                case "82":
                    result = 0x82;
                    break;
                case "83":
                    result = 0x83;
                    break;
                case "84":
                    result = 0x84;
                    break;
                case "85":
                    result = 0x85;
                    break;
                case "86":
                    result = 0x86;
                    break;
                case "87":
                    result = 0x87;
                    break;
                case "88":
                    result = 0x88;
                    break;
                case "89":
                    result = 0x89;
                    break;
                case "8A":
                    result = 0x8A;
                    break;
                case "8B":
                    result = 0x8B;
                    break;
                case "8C":
                    result = 0x8C;
                    break;
                case "8D":
                    result = 0x8D;
                    break;
                case "8E":
                    result = 0x8E;
                    break;
                case "8F":
                    result = 0x8F;
                    break;
                case "90":
                    result = 0x90;
                    break;
                case "91":
                    result = 0x91;
                    break;
                case "92":
                    result = 0x92;
                    break;
                case "93":
                    result = 0x93;
                    break;
                case "94":
                    result = 0x94;
                    break;
                case "95":
                    result = 0x95;
                    break;
                case "96":
                    result = 0x96;
                    break;
                case "97":
                    result = 0x97;
                    break;
                case "98":
                    result = 0x98;
                    break;
                case "99":
                    result = 0x99;
                    break;
                case "9A":
                    result = 0x9A;
                    break;
                case "9B":
                    result = 0x9B;
                    break;
                case "9C":
                    result = 0x9C;
                    break;
                case "9D":
                    result = 0x9D;
                    break;
                case "9E":
                    result = 0x9E;
                    break;
                case "9F":
                    result = 0x9F;
                    break;
                case "A0":
                    result = 0xA0;
                    break;
                case "A1":
                    result = 0xA1;
                    break;
                case "A2":
                    result = 0xA2;
                    break;
                case "A3":
                    result = 0xA3;
                    break;
                case "A4":
                    result = 0xA4;
                    break;
                case "A5":
                    result = 0xA5;
                    break;
                case "A6":
                    result = 0xA6;
                    break;
                case "A7":
                    result = 0xA7;
                    break;
                case "A8":
                    result = 0xA8;
                    break;
                case "A9":
                    result = 0xA9;
                    break;
                case "AA":
                    result = 0xAA;
                    break;
                case "AB":
                    result = 0xAB;
                    break;
                case "AC":
                    result = 0xAC;
                    break;
                case "AD":
                    result = 0xAD;
                    break;
                case "AE":
                    result = 0xAE;
                    break;
                case "AF":
                    result = 0xAF;
                    break;
                case "B0":
                    result = 0xB0;
                    break;
                case "B1":
                    result = 0xB1;
                    break;
                case "B2":
                    result = 0xB2;
                    break;
                case "B3":
                    result = 0xB3;
                    break;
                case "B4":
                    result = 0xB4;
                    break;
                case "B5":
                    result = 0xB5;
                    break;
                case "B6":
                    result = 0xB6;
                    break;
                case "B7":
                    result = 0xB7;
                    break;
                case "B8":
                    result = 0xB8;
                    break;
                case "B9":
                    result = 0xB9;
                    break;
                case "BA":
                    result = 0xBA;
                    break;
                case "BB":
                    result = 0xBB;
                    break;
                case "BC":
                    result = 0xBC;
                    break;
                case "BD":
                    result = 0xBD;
                    break;
                case "BE":
                    result = 0xBE;
                    break;
                case "BF":
                    result = 0xBF;
                    break;
                case "C0":
                    result = 0xC0;
                    break;
                case "C1":
                    result = 0xC1;
                    break;
                case "C2":
                    result = 0xC2;
                    break;
                case "C3":
                    result = 0xC3;
                    break;
                case "C4":
                    result = 0xC4;
                    break;
                case "C5":
                    result = 0xC5;
                    break;
                case "C6":
                    result = 0xC6;
                    break;
                case "C7":
                    result = 0xC7;
                    break;
                case "C8":
                    result = 0xC8;
                    break;
                case "C9":
                    result = 0xC9;
                    break;
                case "CA":
                    result = 0xCA;
                    break;
                case "CB":
                    result = 0xCB;
                    break;
                case "CC":
                    result = 0xCC;
                    break;
                case "CD":
                    result = 0xCD;
                    break;
                case "CE":
                    result = 0xCE;
                    break;
                case "CF":
                    result = 0xCF;
                    break;
                case "D0":
                    result = 0xD0;
                    break;
                case "D1":
                    result = 0xD1;
                    break;
                case "D2":
                    result = 0xD2;
                    break;
                case "D3":
                    result = 0xD3;
                    break;
                case "D4":
                    result = 0xD4;
                    break;
                case "D5":
                    result = 0xD5;
                    break;
                case "D6":
                    result = 0xD6;
                    break;
                case "D7":
                    result = 0xD7;
                    break;
                case "D8":
                    result = 0xD8;
                    break;
                case "D9":
                    result = 0xD9;
                    break;
                case "DA":
                    result = 0xDA;
                    break;
                case "DB":
                    result = 0xDB;
                    break;
                case "DC":
                    result = 0xDC;
                    break;
                case "DD":
                    result = 0xDD;
                    break;
                case "DE":
                    result = 0xDE;
                    break;
                case "DF":
                    result = 0xDF;
                    break;
                case "E0":
                    result = 0xE0;
                    break;
                case "E1":
                    result = 0xE1;
                    break;
                case "E2":
                    result = 0xE2;
                    break;
                case "E3":
                    result = 0xE3;
                    break;
                case "E4":
                    result = 0xE4;
                    break;
                case "E5":
                    result = 0xE5;
                    break;
                case "E6":
                    result = 0xE6;
                    break;
                case "E7":
                    result = 0xE7;
                    break;
                case "E8":
                    result = 0xE8;
                    break;
                case "E9":
                    result = 0xE9;
                    break;
                case "EA":
                    result = 0xEA;
                    break;
                case "EB":
                    result = 0xEB;
                    break;
                case "EC":
                    result = 0xEC;
                    break;
                case "ED":
                    result = 0xED;
                    break;
                case "EE":
                    result = 0xEE;
                    break;
                case "EF":
                    result = 0xEF;
                    break;
                case "F0":
                    result = 0xF0;
                    break;
                case "F1":
                    result = 0xF1;
                    break;
                case "F2":
                    result = 0xF2;
                    break;
                case "F3":
                    result = 0xF3;
                    break;
                case "F4":
                    result = 0xF4;
                    break;
                case "F5":
                    result = 0xF5;
                    break;
                case "F6":
                    result = 0xF6;
                    break;
                case "F7":
                    result = 0xF7;
                    break;
                case "F8":
                    result = 0xF8;
                    break;
                case "F9":
                    result = 0xF9;
                    break;
                case "FA":
                    result = 0xFA;
                    break;
                case "FB":
                    result = 0xFB;
                    break;
                case "FC":
                    result = 0xFC;
                    break;
                case "FD":
                    result = 0xFD;
                    break;
                case "FE":
                    result = 0xFE;
                    break;
                case "FF":
                    result = 0xFF;
                    break;
                default:
                    break;
            }
            return result;
        }

        #endregion

        private void ConvertStrToX16(string data)
        {
            try
            {
                ConvertStrToX16Impl(data);
            }
            catch { }
        }

        private void ConvertStrToX16Impl(string data)
        {
            if(string.IsNullOrEmpty(data))
            {
                return;
            }
            string couple = string.Empty;
            char[] array = data.ToArray();
            List<byte> result = new List<byte>();
            for(int i=0;i<array.Length;i++)
            {
                char item = array[i];
                if(item == ' ' || item == ',')
                {
                    continue;
                }
                int len = couple.Length;
                if(len == 2)
                {
                    byte buff = ConvertStrToX16Single(couple);
                    result.Add(buff);
                    couple = string.Empty;
                }
                couple += array[i];
            }
            if(!string.IsNullOrEmpty(couple))
            {
                byte buff = ConvertStrToX16Single(couple);
                result.Add(buff);
            }
            this.MyData = result.ToArray();
        }


    }
}
