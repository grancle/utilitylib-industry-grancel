﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class DictionaryHelper
    {
        public static void DoSomethingForEach<T>(this Dictionary<string, T> list, Action<T> action)
        {
            if (list == null || list.Count == 0 || action == null)
            {
                return;
            }
            list.DoSomethingForEach((item) =>
            {
                if (string.IsNullOrEmpty(item.Key) || item.Value == null) return;
                action.Invoke(item.Value);
            });
        }

        public static bool DoSomethingForEachBreak<T>(this Dictionary<string, T> list, Action<T> action, string name)
        {
            if (list == null || action == null || string.IsNullOrEmpty(name))
            {
                return false;
            }
            return list.DoSomethingForEachBreak((item) =>
            {
                if (string.IsNullOrEmpty(item.Key) || item.Value == null) return;
                action.Invoke(item.Value);
            }, (item) =>
            {
                return item.Key == name && item.Value != null;
            });
        }

        public static TValue FirstValueCompareWithKey<TKey,TValue>(this Dictionary<TKey, TValue> list, IComparer<TKey> comparer)
        {
            if (list == null || list.Count ==0 || comparer == null)
            {
                return default(TValue);
            }

            TKey key = default(TKey);
            TValue value = default(TValue);
            int cnt = 0;
            list.DoSomethingForEach((item) =>
            {
                if(cnt == 0)
                {
                    key = item.Key;
                    value = item.Value;
                    cnt++;
                    return;
                }

                int sort = comparer.Compare(key, item.Key);
                if(sort<=0)
                {
                    return;
                }

                key = item.Key;
                value = item.Value;

            });

            return value;
        }

        public static TValue FirstValueCompareWithValue<TKey, TValue>(this Dictionary<TKey, TValue> list, IComparer<TValue> comparer)
        {
            if (list == null || list.Count == 0 || comparer == null)
            {
                return default(TValue);
            }

            //TKey key = default(TKey);
            TValue value = default(TValue);
            int cnt = 0;
            list.DoSomethingForEach((item) =>
            {
                if (cnt == 0)
                {
                    //key = item.Key;
                    value = item.Value;
                    cnt++;
                    return;
                }

                int sort = comparer.Compare(value, item.Value);
                if (sort <= 0)
                {
                    return;
                }

                //key = item.Key;
                value = item.Value;

            });

            return value;
        }
    }
}
