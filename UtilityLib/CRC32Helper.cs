﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityLib.CRC;

namespace UtilityLib
{
    /// <summary>
    /// chksum func
    /// Name Polynomial Initial FinalXor   InputReflected ResultReflected
    /// CRC8                 0x07       0x00       0x00       false          false
    /// CRC8_SAE_J1850       0x1D       0xFF       0xFF       false          false
    /// CRC8_SAE_J1850_ZERO  0x1D       0x00       0x00       false          false
    /// CRC8_8H2F            0x2F       0xFF       0xFF       false          false
    /// CRC8_CDMA2000        0x9B       0xFF       0x00       false          false
    /// CRC8_DARC            0x39       0x00       0x00       true           true
    /// CRC8_DVB_S2          0xD5       0x00       0x00       false          false
    /// CRC8_EBU             0x1D       0xFF       0x00       true           true
    /// CRC8_ICODE           0x1D       0xFD       0x00       false          false
    /// CRC8_ITU             0x07       0x00       0x55       false          false
    /// CRC8_MAXIM           0x31       0x00       0x00       true           true
    /// CRC8_ROHC            0x07       0xFF       0x00       true           true
    /// CRC8_WCDMA           0x9B       0x00       0x00       true           true
    /// CRC16_CCIT_ZERO      0x1021     0x0000     0x0000     false          false
    /// CRC16_ARC            0x8005     0x0000     0x0000     true           true
    /// CRC16_AUG_CCITT      0x1021     0x1D0F     0x0000     false          false
    /// CRC16_BUYPASS        0x8005     0x0000     0x0000     false          false
    /// CRC16_CCITT_FALSE    0x1021     0xFFFF     0x0000     false          false
    /// CRC16_CDMA2000       0xC867     0xFFFF     0x0000     false          false
    /// CRC16_DDS_110        0x8005     0x800D     0x0000     false          false
    /// CRC16_DECT_R         0x0589     0x0000     0x0001     false          false
    /// CRC16_DECT_X         0x0589     0x0000     0x0000     false          false
    /// CRC16_DNP            0x3D65     0x0000     0xFFFF     true           true
    /// CRC16_EN_13757       0x3D65     0x0000     0xFFFF     false          false
    /// CRC16_GENIBUS        0x1021     0xFFFF     0xFFFF     false          false
    /// CRC16_MAXIM          0x8005     0x0000     0xFFFF     true           true
    /// CRC16_MCRF4XX        0x1021     0xFFFF     0x0000     true           true
    /// CRC16_RIELLO         0x1021     0xB2AA     0x0000     true           true
    /// CRC16_T10_DIF        0x8BB7     0x0000     0x0000     false          false
    /// CRC16_TELEDISK       0xA097     0x0000     0x0000     false          false
    /// CRC16_TMS37157       0x1021     0x89EC     0x0000     true           true
    /// CRC16_USB            0x8005     0xFFFF     0xFFFF     true           true
    /// CRC16_A              0x1021     0xC6C6     0x0000     true           true
    /// CRC16_KERMIT         0x1021     0x0000     0x0000     true           true
    /// CRC16_MODBUS         0x8005     0xFFFF     0x0000     true           true
    /// CRC16_X_25           0x1021     0xFFFF     0xFFFF     true           true
    /// CRC16_XMODEM         0x1021     0x0000     0x0000     false          false
    /// CRC32                0x04C11DB7 0xFFFFFFFF 0xFFFFFFFF true           true
    /// CRC32_BZIP2          0x04C11DB7 0xFFFFFFFF 0xFFFFFFFF false          false
    /// CRC32_C              0x1EDC6F41 0xFFFFFFFF 0xFFFFFFFF true           true
    /// CRC32_D              0xA833982B 0xFFFFFFFF 0xFFFFFFFF true           true
    /// CRC32_MPEG2          0x04C11DB7 0xFFFFFFFF 0x00000000 false          false
    /// CRC32_POSIX          0x04C11DB7 0x00000000 0xFFFFFFFF false          false
    /// CRC32_Q              0x814141AB 0x00000000 0x00000000 false          false
    /// CRC32_JAMCRC         0x04C11DB7 0xFFFFFFFF 0x00000000 true           true
    /// CRC32_XFER           0x000000AF 0x00000000 0x00000000 false          false
    /// </summary>
    public class CRC32Helper
    {
        public CRC32Helper(byte[] MySrcData)
        {
            this.MySrcData = MySrcData;
        }

        public CRC32Helper(string data, Encoding encoding)
        {
            this.MySrcData = Formulars.Instance.ConvertStrToByte(data, encoding);
        }

        public byte[] MySrcData { get; private set; }

        public bool IsSrcEmpty
        {
            get
            {
                return MySrcData == null || MySrcData.Length == 0;
            }
        }

        public uint Len
        {
            get
            {
                if(IsSrcEmpty)
                {
                    return 0;
                }
                return (uint)MyCharArray.Length;
            }
        }

        public char[] MyCharArray
        {
            get
            {
                if(IsSrcEmpty)
                {
                    return null;
                }

                return ByteConverter.Instance.ConvertByteToCharArray(MySrcData);
            }
        }

        //private delegate ushort del_crc_16_func(ushort crc, char[] buf, uint len);
        //private delegate uint del_crc_32_func(uint crc, byte[] buf, uint len);

        //private byte[] crc_16_impl(uint len, del_crc_16_func func, ushort initial)
        //{
        //    ushort crc = func(initial, MyCharArray, Len);
        //    byte[] result = ByteConverter.Instance.ConvertUintToByte(crc, len);
        //    return result;
        //}

        //private byte[] crc_16_reverse_impl(uint len, del_crc_16_func func, ushort initial)
        //{
        //    byte[] result = crc_16_impl(len, func, initial);
        //    result = ByteConverter.Instance.Reverse(result);
        //    return result;
        //}

        //private byte[] crc_32_impl(uint len, del_crc_32_func func, uint initial)
        //{
        //    uint crc = func(initial, MySrcData, Len);
        //    byte[] result = ByteConverter.Instance.ConvertUintToByte(crc, len);
        //    result = ByteConverter.Instance.Reverse(result);
        //    return result;
        //}

        //private byte[] crc_32_reverse_impl(uint len, del_crc_32_func func, uint initial)
        //{
        //    byte[] result = crc_32_impl(len, func, initial);
        //    result = ByteConverter.Instance.Reverse(result);
        //    return result;
        //}

        public byte[] crc32()
        {
            uint crc = new _CRC32_H_().crc32(this.MySrcData);
            byte[] result = ByteConverter.Instance.ConvertUintToByte(crc, 4);
            return result;
            //return crc_32_impl(len, new _CRC32_H_().crc32, 0xFFFFFFFF);
        }

        //public byte[] crc32_reverse(uint len)
        //{
        //    return crc_32_reverse_impl(len, new _CRC32_H_().crc32, 0xFFFFFFFF);
        //}

        //public byte[] crc16_arc(uint len)
        //{
        //    return crc_16_impl(len, new _CRC16_ARC_H_().crc16_arc, 0x0000);
        //}

        //public byte[] crc16_arc_reverse(uint len)
        //{
        //    return crc_16_reverse_impl(len, new _CRC16_ARC_H_().crc16_arc, 0x0000);
        //}

        public byte[] crc16_modbus(bool is_lower_first = false)
        {
            uint crc = new _CRC16_MODBUS_H().crc16_modbus(this.MySrcData);
            byte[] result = ByteConverter.Instance.ConvertUintToByte(crc, 2);
            if(is_lower_first)
            {
                result = ByteConverter.Instance.Reverse(result);
            }
            return result;
        }
    }
}
