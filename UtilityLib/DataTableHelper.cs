﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class DataTableHelper
    {
        public static DataTableHelper Instance { get; } = new DataTableHelper();
        public DataTableHelper()
        {
          
        }
        public  DataTable CopyData(DataTable sourceTable, DataTable targetTable)
        {
            foreach (DataRow sourceRow in sourceTable.Rows)
            {
                DataRow targetRow = targetTable.NewRow();
                foreach (DataColumn col in targetTable.Columns)
                {
                    string columnName = col.ColumnName;
                    targetRow[columnName] = ConvertValue(sourceRow[columnName], col.DataType);
                }
                targetTable.Rows.Add(targetRow);
            }
            return targetTable;
        }
        private object ConvertValue(object value, Type targetType)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString()) || value.ToString() == "{}")
            {
                // 处理空值，为不同的目标类型提供默认值
                if (targetType == typeof(int) || targetType == typeof(decimal))
                {
                    return 0; // 默认值为0
                }
                else if (targetType == typeof(string))
                {
                    return string.Empty; // 默认值为空字符串
                }
                // 还可以添加其他类型的处理
                // ...

                // 如果没有匹配的目标类型，你可以根据需要返回一个适当的默认值
            }

            else
            {
                // 非空值时，尝试进行类型转换
                try
                {

                    return Convert.ChangeType(value, targetType);
                }
                catch (InvalidCastException)
                {
                    // 处理无法转换的情况，或者返回一个适当的默认值
                    // 例如，对于无法转换的情况，可以返回目标类型的默认值
                    if (targetType == typeof(int))
                    {
                        return 0;
                    }
                    // 还可以添加其他类型的处理
                    // ...
                }
            }

            // 如果没有匹配的处理，你可以根据需要返回一个适当的默认值
            return null; // 或者其他默认值
        }

    }
}
