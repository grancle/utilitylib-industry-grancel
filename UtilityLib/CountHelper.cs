﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public class CountHelper
    {
        public CountHelper(int OverloadValue, int OverbackValue =1)
        {
            this.OverloadValue = OverloadValue;
            this.OverbackValue = OverbackValue;
        }

        private volatile int _cnt = 0;

        public int OverloadValue { get; private set; }
        public int OverbackValue { get; private set; }
        public void Count()
        {
            _cnt++;
            if(_cnt > OverloadValue)
            {
                _cnt = OverbackValue;
            }
            CountEvent?.Invoke(this, null);
        }

        public void Reset()
        {
            _cnt = 0;
        }

        public event EventHandler CountEvent;

        public int CurrentCount
        {
            get
            {
                return _cnt;
            }
        }
    }
}
