﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityLib
{
    public static class DateTimeHelper
    {
        public static DateTime ToNextDate(this DateTime date, int hour, int minute)
        {
            Formulars.Instance.CheckValue(ref hour, 0, 23);
            Formulars.Instance.CheckValue(ref minute, 0, 59);

            DateTime result = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0);

            int interval_hour = hour - date.Hour;
            if(interval_hour <0)
            {
                interval_hour += 24;
            }

            int interval_minute = minute - date.Minute;

            if(interval_minute <= 0 && interval_hour ==0)
            {
                result = result.AddDays(1);
            }


            result = result.AddHours(interval_hour);
            result = result.AddMinutes(interval_minute);

            return result;
        }

        public static string ToStringNormal(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }
    }
}
