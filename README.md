# UtilityLib.Industry.Grancel

#### 介绍
为工业类软件专用的通用方法处理库。

#### 建议&疑问
任何建议或疑问请发送到lijiaan2@grancel.com

#### 捐赠
捐赠请扫以下二维码，感谢您的支持~!

<img src="https://foruda.gitee.com/images/1702454843769519613/2bb9742f_9137086.jpeg" width="180px">
